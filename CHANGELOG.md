## [1.3.4](https://gitlab.com/tapioca-ufrn/multiprova/particularizacao/compare/1.3.3...1.3.4) (2021-03-18)


### Config

* ajusta babel para compatibilidade com webpack ([d8338b3](https://gitlab.com/tapioca-ufrn/multiprova/particularizacao/commit/d8338b38442fb93613afe97541a2400085c6c958))

## [1.3.3](https://gitlab.com/tapioca-ufrn/multiprova/particularizacao/compare/1.3.2...1.3.3) (2021-03-12)


### Bug Fixes

* adiciona artefato de lib ([86ff2ec](https://gitlab.com/tapioca-ufrn/multiprova/particularizacao/commit/86ff2ec55ed55c0bb176e7a13bac84f7422a9d3b))

## [1.3.2](https://gitlab.com/tapioca-ufrn/multiprova/particularizacao/compare/1.3.1...1.3.2) (2021-03-12)


### Code Refactoring

* cria versão em commonjs da lib ([6b169c1](https://gitlab.com/tapioca-ufrn/multiprova/particularizacao/commit/6b169c15c6333ede5eb69c745fd73e8689cb0e04))
* remove código comentado ([6158f79](https://gitlab.com/tapioca-ufrn/multiprova/particularizacao/commit/6158f795f4b022d849c059bdbb9fe5ecd38e2ef1))
* resolve conflitos ([909157c](https://gitlab.com/tapioca-ufrn/multiprova/particularizacao/commit/909157c3ad5d9e0e1a5461b7a85aead6a8d97b41))


### Config

* adiciona configurações do sonar ([9acdcf0](https://gitlab.com/tapioca-ufrn/multiprova/particularizacao/commit/9acdcf0040287c59f00725b584ac70261b22d35f))

## [1.3.1](https://gitlab.com/tapioca-ufrn/multiprova/particularizacao/compare/1.3.0...1.3.1) (2020-12-15)


### Bug Fixes

* retira arquivo de configuração dos testes ([8341482](https://gitlab.com/tapioca-ufrn/multiprova/particularizacao/commit/8341482838088678cca0e151cf0672b853165f3b))
