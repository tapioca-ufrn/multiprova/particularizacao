import { validarEntradas } from '../../utils/validarEntradas'
import { create, all } from 'mathjs'
const math = create(all)

const ajustarValidacao = (a) => {
  return {
    nomeDaFuncao: 'trapezio',
    requisitos: [
      {
        nome: 'f',
        tipo: 'Function',
        requerido: true,
      },
      {
        nome: 'a',
        tipo: 'number',
        requerido: true,
      },
      {
        nome: 'b',
        tipo: 'number',
        requerido: true,
        condicoes: [
          {
            operador: '>',
            valor: a,
          },
        ],
      },
      {
        nome: 'n',
        tipo: 'number',
        requerido: true,
      },
    ],
  }
}

export const trapezio = (f, a, b, n) => {
  const entradas = {
    f: f,
    a: a,
    b: b,
    n: n,
  }
  const validacao = ajustarValidacao(a)
  validarEntradas(entradas, validacao)
  n = math.round(n)

  const h = (b - a) / n
  let soma = 0
  let x = a
  for (let i = 1; i <= n; i++) {
    soma = soma + f(x) + f(x + h)
    x = x + h
  }
  return (h / 2) * soma
}
