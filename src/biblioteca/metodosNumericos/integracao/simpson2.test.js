import { simpson2 } from './simpson2'
import { create, all } from 'mathjs'

const math = create(all)
math.import({
  simpson2: simpson2,
})

describe('Teste de saída de valores', () => {
  it('Regra de Simpson 3/8', () => {
    let resultadoParticularizacao = math.evaluate('simpson2(f(x)=sqrt(1-x^2)*4,-1,0,4)')
    resultadoParticularizacao = math.round(resultadoParticularizacao, 4)
    expect(resultadoParticularizacao).toEqual(3.1281)
  })
})
