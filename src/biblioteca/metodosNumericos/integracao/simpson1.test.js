import { simpson1 } from './simpson1'
import { create, all } from 'mathjs'

const math = create(all)
math.import({
  simpson1: simpson1,
})

describe('Teste de saída de valores', () => {
  it('Regra de Simpson 1/3', () => {
    let resultadoParticularizacao = math.evaluate('simpson1(f(x)=sqrt(1-x^2)*4,-1,0,8)')
    resultadoParticularizacao = math.round(resultadoParticularizacao, 4)
    expect(resultadoParticularizacao).toEqual(3.1344)
  })
})
