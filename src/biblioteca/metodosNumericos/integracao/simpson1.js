import { validarEntradas } from '../../utils/validarEntradas'
import { create, all } from 'mathjs'
const math = create(all)

const ajustarValidacao = (a) => {
  return {
    nomeDaFuncao: 'trapezio',
    requisitos: [
      {
        nome: 'f',
        tipo: 'Function',
        requerido: true,
      },
      {
        nome: 'a',
        tipo: 'number',
        requerido: true,
      },
      {
        nome: 'b',
        tipo: 'number',
        requerido: true,
        condicoes: [
          {
            operador: '>',
            valor: a,
          },
        ],
      },
      {
        nome: 'n',
        tipo: 'number',
        requerido: true,
        condicoes: [
          {
            operador: '>',
            valor: 0,
          },
        ],
      },
    ],
  }
}

export const simpson1 = (f, a, b, n) => {
  const entradas = {
    f: f,
    a: a,
    b: b,
    n: n,
  }
  const validacao = ajustarValidacao(a)
  validarEntradas(entradas, validacao)
  n = math.round(n)

  const h = (b - a) / (2 * n)
  let soma = 0
  let x = a
  for (let i = 1; i <= n; i++) {
    soma = soma + f(x) + 4 * f(x + h) + f(x + 2 * h)
    x = x + 2 * h
  }
  return (h / 3) * soma
}
