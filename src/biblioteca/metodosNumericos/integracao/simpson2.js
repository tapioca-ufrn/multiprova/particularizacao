import { validarEntradas } from '../../utils/validarEntradas'
import { create, all } from 'mathjs'
const math = create(all)

const ajustarValidacao = (a) => {
  return {
    nomeDaFuncao: 'trapezio',
    requisitos: [
      {
        nome: 'f',
        tipo: 'Function',
        requerido: true,
      },
      {
        nome: 'a',
        tipo: 'number',
        requerido: true,
      },
      {
        nome: 'b',
        tipo: 'number',
        requerido: true,
        condicoes: [
          {
            operador: '>',
            valor: a,
          },
        ],
      },
      {
        nome: 'n',
        tipo: 'number',
        requerido: true,
        condicoes: [
          {
            operador: '>',
            valor: 0,
          },
        ],
      },
    ],
  }
}

export const simpson2 = (f, a, b, n) => {
  const entradas = {
    f: f,
    a: a,
    b: b,
    n: n,
  }
  const validacao = ajustarValidacao(a)
  validarEntradas(entradas, validacao)
  n = math.round(n)

  const h = (b - a) / (3 * n)
  let soma = 0
  let x = a
  for (let i = 1; i <= n; i++) {
    soma = soma + f(x) + 3 * f(x + h) + 3 * f(x + 2 * h) + f(x + 3 * h)
    x = x + 3 * h
  }
  return ((3 * h) / 8) * soma
}
