import { trapezio } from './trapezio'
import { create, all } from 'mathjs'

const math = create(all)
math.import({
  trapezio: trapezio,
})

describe('Teste de saída de valores', () => {
  it('Regra do trapézio', () => {
    let resultadoParticularizacao = math.evaluate('trapezio(f(x)=sqrt(1-x^2)*4,-1,0,301)')
    resultadoParticularizacao = math.round(resultadoParticularizacao, 4)
    expect(resultadoParticularizacao).toEqual(3.1414)
  })
})
