import { secante } from './secante'
import { create, all } from 'mathjs'

const math = create(all)
math.import({
  secante: secante,
})

describe('Teste de saída de valores', () => {
  it('Raiz encontrada. Parada pelo erro relativo. Saída Raíz', () => {
    let resultadoParticularizacao = math.evaluate('secante(f(x)=x^2-1, 0, 0.5, 8, 2)')
    resultadoParticularizacao = math.round(resultadoParticularizacao, 4)
    expect(resultadoParticularizacao).toEqual(0.9997)
  })
  it('Raiz encontrada. Parada pelo número de iterações. Saída Erro', () => {
    let resultadoParticularizacao = math.evaluate('secante(f(x)=x^2-1, 0, 0.5, 2, 8, 1)')
    resultadoParticularizacao = math.round(resultadoParticularizacao, 5)
    expect(resultadoParticularizacao).toEqual(1.5)
  })
  it('Raiz encontrada. Parada pelo número de iterações. Saída nItercações', () => {
    let resultadoParticularizacao = math.evaluate('secante(f(x)=x^2-1, 0, 0.5, 2, 8, 2)')
    expect(resultadoParticularizacao).toEqual(2)
  })
})
