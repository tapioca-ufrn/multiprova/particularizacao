import { falsaPosicao } from './falsaPosicao'
import { create, all } from 'mathjs'

const math = create(all)
math.import({
  falsaPosicao: falsaPosicao,
})

describe('Argumentos incorretos', () => {
  it('Teste de Bolzanno falhou', () => {
    try {
      math.evaluate('falsaPosicao(f(x)=x+0.5, 0, 1, 3, 3 )')
    } catch (error) {
      expect(error.message).toContain('menor do que zero')
    }
  })
})

describe('Teste de saída de valores', () => {
  it('Raiz encontrada. Parada pelo número de iterações. Saída Raíz', () => {
    let resultadoParticularizacao = math.evaluate('falsaPosicao(f(x)=x^2-1, 0.8, 1.4, 5, 8)')
    resultadoParticularizacao = math.round(resultadoParticularizacao, 5)
    //console.log(resultadoParticularizacao)
    expect(resultadoParticularizacao).toEqual(0.99997)
  })
  it('Raiz encontrada. Parada pelo número de iterações. Saída Raíz', () => {
    let resultadoParticularizacao = math.evaluate('falsaPosicao(f(x)=x^2-1, -1.4, -0.8, 5, 8)')
    resultadoParticularizacao = math.round(resultadoParticularizacao, 5)
    //console.log(resultadoParticularizacao)
    expect(resultadoParticularizacao).toEqual(-0.99997)
  })
  it('Raiz encontrada. Parada pelo número de iterações. Saída Erro', () => {
    let resultadoParticularizacao = math.evaluate('falsaPosicao(f(x)=x^2-1, 0.8, 1.4, 2, 8, 1)')
    resultadoParticularizacao = math.round(resultadoParticularizacao, 2)
    expect(resultadoParticularizacao).toEqual(0.03)
  })
  it('Raiz encontrada. Parada pelo número de iterações. Saída nItercações', () => {
    let resultadoParticularizacao = math.evaluate('falsaPosicao(f(x)=x^2-1, 0.8, 1.4, 2, 8, 2)')
    expect(resultadoParticularizacao).toEqual(2)
  })
})
