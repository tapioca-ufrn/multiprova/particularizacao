import { validarEntradas } from '../../utils/validarEntradas'
import { create, all } from 'mathjs'
const math = create(all)

const validacao = {
  nomeDaFuncao: 'secante',
  requisitos: [
    {
      nome: 'f',
      tipo: 'Function',
      requerido: true,
    },
    {
      nome: 'x0',
      tipo: 'number',
      requerido: true,    
    },
    {
      nome: 'x1',
      tipo: 'number',
      requerido: true,      
    },
    {
      nome: 'numMaxIt',
      tipo: 'number',
      requerido: true,
      condicoes: [
        {
          operador: '<=',
          valor: 300,
        },
      ],      
    },
    {
      nome: 'precisao',
      tipo: 'number',
      requerido: true,
    },
    {
      nome: 'saida',
      tipo: 'number',
      requerido: false,
    },
  ],
}

export const secante = (f, x0, x1, numMaxIt, precisao, saida = 0) => {
  const entradas = {
    f: f,
    x0: x0,
    x1: x1,
    numMaxIt: numMaxIt,
    precisao: precisao,
    saida: saida,
  }
  validarEntradas(entradas, validacao)

  let erro = 100
  let nIteracao = 0
  let xn
  while (erro > math.pow(10, -precisao) && nIteracao < numMaxIt) {
    xn = x1 - (f(x1) * (x0 - x1)) / (f(x0) - f(x1))
    x0 = x1
    x1 = xn
    erro = math.abs((x1 - x0) / x1)
    nIteracao++
  }

  switch (saida) {
    case 1:
      return erro
    case 2:
      return nIteracao
    default:
      return x1
  }
}
