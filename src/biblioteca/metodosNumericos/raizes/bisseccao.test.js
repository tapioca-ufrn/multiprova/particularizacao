import { bisseccao } from './bisseccao'
import { create, all } from 'mathjs'

const math = create(all)
math.import({
  bisseccao: bisseccao,
})

describe('Argumentos incorretos', () => {
  it('Teste de Bolzanno falhou', () => {
    try {
      math.evaluate('bisseccao(f(x)=x+0.5, 0, 1, 3, 3 )')
    } catch (error) {
      expect(error.message).toContain('menor do que zero')
    }
  })
})

describe('Teste de saída de valores', () => {
  it('Raiz encontrada. Parada pelo número de iterações. Saída Raíz', () => {
    let resultadoParticularizacao = math.evaluate('bisseccao(f(x)=x^2-1, a, 1.4, 2, 8)', { a: 0.8 })
    resultadoParticularizacao = math.round(resultadoParticularizacao, 2)
    expect(resultadoParticularizacao).toEqual(0.95)
  })
  it('Raiz encontrada. Parada pelo número de iterações. Saída Erro', () => {
    let resultadoParticularizacao = math.evaluate('bisseccao(f(x)=x^2-1, a, 1.4, 2, 8, 1)', { a: 0.8 })
    resultadoParticularizacao = math.round(resultadoParticularizacao, 2)
    expect(resultadoParticularizacao).toEqual(0.16)
  })
  it('Raiz encontrada. Parada pelo número de iterações. Saída nItercações', () => {
    let resultadoParticularizacao = math.evaluate('bisseccao(f(x)=x^2-1, a, 1.4, 2, 8, 2)', { a: 0.8 })
    expect(resultadoParticularizacao).toEqual(2)
  })
})
