import { validarEntradas } from '../../utils/validarEntradas'
import { create, all } from 'mathjs'
const math = create(all)

const validacao = {
  nomeDaFuncao: 'newton',
  requisitos: [
    {
      nome: 'f',
      tipo: 'Function',
      requerido: true,
    },
    {
      nome: 'df',
      tipo: 'Function',
      requerido: true,
    },
    {
      nome: 'x0',
      tipo: 'number',
      requerido: true,      
    },
    {
      nome: 'numMaxIt',
      tipo: 'number',
      requerido: true,
      condicoes: [
        {
          operador: '<=',
          valor: 300,
        },
      ],      
    },
    {
      nome: 'precisao',
      tipo: 'number',
      requerido: true,
    },
    {
      nome: 'saida',
      tipo: 'number',
      requerido: false,
    },
  ],
}

export const newton = (f, df, x0, numMaxIt, precisao, saida = 0) => {
  const entradas = {
    f: f,
    df: df,
    x0: x0,
    numMaxIt: numMaxIt,
    precisao: precisao,
    saida: saida,
  }
  validarEntradas(entradas, validacao)

  let x = x0
  let xOld
  let erro = 100
  let nIteracao = 0
  while (erro > math.pow(10, -precisao) && nIteracao < numMaxIt) {
    xOld = x
    x = x - f(x) / df(x)
    erro = math.abs((x - xOld) / x)
    nIteracao++
  }

  switch (saida) {
    case 1:
      return erro
    case 2:
      return nIteracao
    default:
      return x
  }
}
