import { newton } from './newton'
import { create, all } from 'mathjs'

const math = create(all)
math.import({
  newton: newton,
})

describe('Teste de saída de valores', () => {
  it('Raiz encontrada. Parada pelo erro relativo. Saída Raíz', () => {
    let resultadoParticularizacao = math.evaluate('newton(f(x)=x^2-1, df(x)=2x, a, 20, 4)', { a: 0.8 })
    resultadoParticularizacao = math.round(resultadoParticularizacao, 2)
    expect(resultadoParticularizacao).toEqual(1)
  })
  it('Raiz encontrada. Parada pelo número de iterações. Saída Erro', () => {
    let resultadoParticularizacao = math.evaluate('newton(f(x)=x^2-1, df(x)=2x, a, 2, 8, 1)', { a: 0.8 })
    resultadoParticularizacao = math.round(resultadoParticularizacao, 2)
    expect(resultadoParticularizacao).toEqual(0.02)
  })
  it('Raiz encontrada. Parada pelo número de iterações. Saída nItercações', () => {
    let resultadoParticularizacao = math.evaluate('newton(f(x)=x^2-1, df(x)=2x, x0, 2, 8, 2)', { x0: 0.8 })
    expect(resultadoParticularizacao).toEqual(2)
  })
})
