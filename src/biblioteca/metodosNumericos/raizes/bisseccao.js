import { validarEntradas } from '../../utils/validarEntradas'
import { create, all } from 'mathjs'
const math = create(all)

const validarBozzano = (f, a, b) => {
  if (f(a) * f(b) >= 0) {
    throw new Error(
      'Função bisseccao(): f(a)*f(b) deve ser menor do que zero.',
    )
  }  
}

const validacao = {
  nomeDaFuncao: 'bisseccao',
  requisitos: [
    {
      nome: 'f',
      tipo: 'Function',
      requerido: true,
    },
    {
      nome: 'a',
      tipo: 'number',
      requerido: true,      
    },
    {
      nome: 'b',
      tipo: 'number',
      requerido: true,      
    },
    {
      nome: 'numMaxIt',
      tipo: 'number',
      requerido: true,
      condicoes: [
        {
          operador: '<=',
          valor: 300,
        },
      ],      
    },
    {
      nome: 'precisao',
      tipo: 'number',
      requerido: true,
    },
    {
      nome: 'saida',
      tipo: 'number',
      requerido: false,
    },
  ],
}

export const bisseccao = (f, a, b, numMaxIt, precisao, saida = 0) => {
  const entradas = {
    f: f,
    a: a,
    b: b,
    numMaxIt: numMaxIt,
    precisao: precisao,
    saida: saida,
  }
  validarEntradas(entradas, validacao)
  validarBozzano(f, a, b)

  let x = a
  let xOld
  let erro = 100
  let nIteracao = 0
  while (erro > math.pow(10, -precisao) && nIteracao < numMaxIt) {
    xOld = x
    x = (a + b) / 2
    if (f(a) * f(x) < 0) b = x
    else a = x
    erro = math.abs((x - xOld) / x)
    nIteracao++
  }

  switch (saida) {
    case 1:
      return erro
    case 2:
      return nIteracao
    default:
      return x
  }
}
