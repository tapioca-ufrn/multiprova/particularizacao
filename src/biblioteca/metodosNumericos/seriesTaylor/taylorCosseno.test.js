import { taylorCosseno } from './taylorCosseno'
import { create, all } from 'mathjs'

const math = create(all)
math.import({
  taylorCosseno: taylorCosseno,
})

describe('Teste de saída de valores', () => {
  it('Série com cinco termos .. aproximação de cos(pi/2) em torno de x = 0', () => {
    let resultadoParticularizacao = math.evaluate('taylorCosseno(0, pi/2, 5)')
    resultadoParticularizacao = math.round(resultadoParticularizacao, 5)
    expect(resultadoParticularizacao).toEqual(0.01997)
  })
})
