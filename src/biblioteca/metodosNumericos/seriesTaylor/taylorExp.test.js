import { taylorExp } from './taylorExp'
import { create, all } from 'mathjs'

const math = create(all)
math.import({
  taylorExp: taylorExp,
})

describe('Teste de saída de valores', () => {
  it('Série com 5 termos .. aproximação de exp(1) em torno de x = 0', () => {
    let resultadoParticularizacao = math.evaluate('taylorExp(0, 1, 5)')
    resultadoParticularizacao = math.round(resultadoParticularizacao, 5)
    expect(resultadoParticularizacao).toEqual(2.70833)
  })
})
