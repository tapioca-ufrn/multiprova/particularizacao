import { taylorSeno } from './taylorSeno'
import { create, all } from 'mathjs'

const math = create(all)
math.import({
  taylorSeno: taylorSeno,
})

describe('Teste de saída de valores', () => {
  it('Série com nove termos .. aproximação de sen(pi/2) em torno de x = 9', () => {
    let resultadoParticularizacao = math.evaluate('taylorSeno(0, pi/2, 9)')
    resultadoParticularizacao = math.round(resultadoParticularizacao, 4)
    expect(resultadoParticularizacao).toEqual(0.9998)
  })
})
