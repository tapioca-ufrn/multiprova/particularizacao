import { validarEntradas } from '../../utils/validarEntradas'
import { create, all } from 'mathjs'
const math = create(all)

const validacao = {
  nomeDaFuncao: 'taylorCosseno',
  requisitos: [
    {
      nome: 'x0',
      tipo: 'number',
      requerido: true,
    },
    {
      nome: 'x',
      tipo: 'number',
      requerido: true,      
    },
    {
      nome: 'n',
      tipo: 'number',
      requerido: true,
      condicoes: [
        {
          operador: '<=',
          valor: 50,
        },
      ],      
    },
  ],
}

export const taylorExp = (x0, x, n) => {
  const entradas = {
    x0: x0,
    x: x,
    n: n,
  }
  validarEntradas(entradas, validacao)

  let pdex = 0
  for (let i = 0; i < n; i++) {
    pdex = pdex + (math.exp(x0) * math.pow(x - x0, i)) / math.factorial(i)
  }
  return pdex
}
