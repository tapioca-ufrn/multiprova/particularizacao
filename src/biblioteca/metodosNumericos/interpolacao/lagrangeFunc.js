import { validarEntradas } from '../../utils/validarEntradas'
import { create, all } from 'mathjs'
const math = create(all)

const validacao = {
  nomeDaFuncao: 'lagrangeFunc',
  requisitos: [
    {
      nome: 'a',
      tipo: 'number',
      requerido: true,
    },
    {
      nome: 'b',
      tipo: 'number',
      requerido: true,
    },
    {
      nome: 'h',
      tipo: 'number',
      requerido: true,
    },
    {
      nome: 'func',
      tipo: 'Function',
      requerido: true,
    },
    {
      nome: 'p',
      tipo: 'number',
      requerido: true,
    },
  ],
}

export const lagrangeFunc = (a, b, h, func, p) => {
  const entradas = {
    a: a,
    b: b,
    h: h,
    func: func,
    p: p,
  }
  validarEntradas(entradas, validacao)

  let x = []
  let y = []
  let cont = 0
  for (let i = a; i <= b; i += h) {
    x[cont] = [i]
    y[cont] = [func(i)]
    cont++
  }

  x = math.matrix(x)
  y = math.matrix(y)
  cont = math.size(x)
  cont = math.subset(cont, math.index(0))
  let s = 0
  for (let i = 0; i < cont; i++) {
    let l = 1

    for (let j = 0; j < cont; j++) {
      if (i != j) {
        l =
          (l * (p - math.subset(x, math.index(j, 0)))) /
          (math.subset(x, math.index(i, 0)) - math.subset(x, math.index(j, 0)))
      }
    }
    s = s + l * math.subset(y, math.index(i, 0))
  }

  return s
}
