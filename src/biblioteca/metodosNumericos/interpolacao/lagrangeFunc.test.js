import { lagrangeFunc } from './lagrangeFunc'
import { create, all } from 'mathjs'

const math = create(all)
math.import({
  lagrangeFunc: lagrangeFunc,
})

describe('Teste de saída de valores', () => {
  it('Interpolação de Lagrange grau 1', () => {
    let resultadoParticularizacao = math.evaluate('lagrangeFunc(1,2,1,f(x)=x+2,1.5)')
    resultadoParticularizacao = math.round(resultadoParticularizacao, 2)
    expect(resultadoParticularizacao).toEqual(3.5)
  })
  it('Interpolação de Lagrange grau 2', () => {
    let resultadoParticularizacao = math.evaluate('lagrangeFunc(1,3,1,f(x)=x^2+2,1.5)')
    resultadoParticularizacao = math.round(resultadoParticularizacao, 2)
    expect(resultadoParticularizacao).toEqual(4.25)
  })
})
