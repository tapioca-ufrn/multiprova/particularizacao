import { poliNewtonFunc } from './poliNewtonFunc'
import { create, all } from 'mathjs'

const math = create(all)
math.import({
  poliNewtonFunc: poliNewtonFunc,
})

describe('Teste de saída de valores', () => {
  it('Interpolação de Newton', () => {
    let resultadoParticularizacao = math.evaluate('poliNewtonFunc(1,2,1,f(x)=x+2,1.5)')
    resultadoParticularizacao = math.round(resultadoParticularizacao, 2)
    expect(resultadoParticularizacao).toEqual(3.5)
  })
  it('Interpolação de Newton grau 2', () => {
    let resultadoParticularizacao = math.evaluate('poliNewtonFunc(1,3,1,f(x)=x^2+2,1.5)')
    resultadoParticularizacao = math.round(resultadoParticularizacao, 2)
    expect(resultadoParticularizacao).toEqual(4.25)
  })
  it('Interpolação de Newton grau 2. Retorno da soma dos elemntos da primeira linha da tabela de diferenças divididas', () => {
    let resultadoParticularizacao = math.evaluate('poliNewtonFunc(1,3,1,f(x)=x^2+2,1.5,1)')
    resultadoParticularizacao = math.round(resultadoParticularizacao, 2)
    expect(resultadoParticularizacao).toEqual(7)
  })
  it('Interpolação de Newton grau 2. Retorno da soma dos elemntos da primeira linha da tabela de diferenças divididas', () => {
    let resultadoParticularizacao = math.evaluate('poliNewtonFunc(1,3,0.5,f(x)=cos(x),1.5,1)')
    resultadoParticularizacao = math.round(resultadoParticularizacao, 2)
    expect(resultadoParticularizacao).toEqual(-0.29)
  })
})
