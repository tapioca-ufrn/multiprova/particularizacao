import { poliNewton } from './poliNewton'
import { create, all } from 'mathjs'

const math = create(all)
math.import({
  poliNewton: poliNewton,
})

describe('Teste de saída de valores', () => {
  it('Interpolação de Newton', () => {
    let resultadoParticularizacao = math.evaluate('poliNewton([[1],[2]], [[3],[4]],1.5)')
    resultadoParticularizacao = math.round(resultadoParticularizacao, 2)
    expect(resultadoParticularizacao).toEqual(3.5)
  })
  it('Interpolação de Newton grau 2', () => {
    let resultadoParticularizacao = math.evaluate('poliNewton([[1],[2],[3]], [[3],[6],[11]],1.5)')
    resultadoParticularizacao = math.round(resultadoParticularizacao, 2)
    expect(resultadoParticularizacao).toEqual(4.25)
  })
  it('Interpolação de Newton grau 2. Retorno da soma dos elemntos da primeira linha da tabela de diferenças divididas', () => {
    let resultadoParticularizacao = math.evaluate('poliNewton([[1],[2],[3]], [[3],[6],[11]],1.5,1)')
    resultadoParticularizacao = math.round(resultadoParticularizacao, 2)
    expect(resultadoParticularizacao).toEqual(7)
  })
})
