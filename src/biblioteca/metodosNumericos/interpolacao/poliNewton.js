import { validarEntradas } from '../../utils/validarEntradas'
import { create, all } from 'mathjs'
const math = create(all)

const validacao = {
  nomeDaFuncao: 'poliNewton',
  requisitos: [
    {
      nome: 'x',
      tipo: 'vetorColuna',
      requerido: true,
    },
    {
      matrizA: 'x',
      matrizB: 'y',
      tipo: 'matrizesMesmaDimensao',
      requerido: true,
    },
    {
      nome: 'p',
      tipo: 'number',
      requerido: true,
    },
    {
      nome: 'saida',
      tipo: 'number',
      requerido: true,
    },
  ],
}

export const tabDiffDiv = (x, y) => {
  x = math.matrix(x)
  y = math.matrix(y)
  let cont = math.size(x)
  cont = math.subset(cont, math.index(0))
  let tab = math.zeros(cont, cont)
  for (let i = 0; i < cont; i++) {
    tab = math.subset(tab, math.index(i, 0), math.subset(y, math.index(i, 0)))
  }
  let tab1, tab2, x1, x2

  for (let i = 0; i < cont - 1; i++) {
    for (let j = 0; j < cont - i - 1; j++) {
      tab1 = math.subset(tab, math.index(j + 1, i))
      tab2 = math.subset(tab, math.index(j, i))
      x1 = math.subset(x, math.index(j + i + 1, 0))
      x2 = math.subset(x, math.index(j, 0))
      tab = math.subset(tab, math.index(j, i + 1), (tab1 - tab2) / (x1 - x2))
    }
  }
  return tab
}

export const poliNewton = (x, y, p, saida = 0) => {
  const entradas = {
    x: x,
    y: y,
    p: p,
    saida: saida,
  }
  validarEntradas(entradas, validacao)

  let tab = tabDiffDiv(x, y)

  x = math.matrix(x)
  y = math.matrix(y)
  let cont = math.size(x)
  cont = math.subset(cont, math.index(0))

  let s
  let l
  let somaBis
  s = math.subset(tab, math.index(0, 0))
  somaBis = math.subset(tab, math.index(0, 0))
  for (let i = 1; i < cont; i++) {
    l = 1
    for (let j = 0; j <= i - 1; j++) {
      l = l * (p - math.subset(x, math.index(j, 0)))
    }
    s = s + l * math.subset(tab, math.index(0, i))
    somaBis += math.subset(tab, math.index(0, i))
  }

  switch (saida) {
    case 1:
      return somaBis
    default:
      return s
  }
}
