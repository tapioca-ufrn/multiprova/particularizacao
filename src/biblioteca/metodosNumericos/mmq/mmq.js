import { validarEntradas } from '../../utils/validarEntradas'
import { create, all } from 'mathjs'
const math = create(all)

let validacao1 = {
  nomeDaFuncao: 'mmq',
  requisitos: [
    {
      nome: 'x',
      tipo: 'vetorColuna',
      requerido: true,
    },
    {
      matrizA: 'x',
      matrizB: 'y',
      tipo: 'matrizesMesmaDimensao',
      requerido: true,
    },
    {
      nome: 'n',
      tipo: 'number',
      requerido: true,
    },
  ],
}

const preparaValidacao = (n, f) => {
  let entradas = {}
  let validacao = {
    nomeDaFuncao: 'mmq',
    requisitos: [],
  }
  for (let i = 0; i < n; i++) {
    entradas['f' + String(i)] = f[i]
    validacao.requisitos.push({
      nome: 'f' + String(i),
      tipo: 'Function',
      requerido: true,
    })
  }
  return [entradas, validacao]
}

const validarNumeroDeFuncoes = (n, args) => {
  if (!(args.length === n || args.length === n + 1)) {
    throw new Error('Função mmq(): a quantidade de funções está incorreta.')
  }
}

const validarParametroSaida = (n, args) => {
  if (args.length > n) {
    const entradas = {
      parametroSaida: args[args.length - 1],
    }
    const validacao = {
      nomeDaFuncao: 'mmq',
      requisitos: [
        {
          nome: 'parametroSaida',
          tipo: 'number',
          requerido: true,
        },
      ],
    }
    validarEntradas(entradas, validacao)
  }
}

const validarTodasEntradas = (x, y, n, args) => {
  let entradas1 = {
    x: x,
    y: y,
    n: n,
  }
  validarEntradas(entradas1, validacao1)

  validarNumeroDeFuncoes(n, args)
  const [entradas, validacao] = preparaValidacao(n, args)
  validarEntradas(entradas, validacao)
  validarParametroSaida(n, args)
}

export const mmq = (x, y, n, ...args) => {
  let nargs = args.length
  let f = args

  validarTodasEntradas(x, y, n, f)

  x = math.matrix(x)
  y = math.matrix(y)
  let cont = math.size(x)
  cont = math.subset(cont, math.index(0))
  let m = math.zeros(cont, n)
  for (let i = 0; i < cont; i++) {
    for (let j = 0; j < n; j++) {
      f = args[j]
      m = math.subset(m, math.index(i, j), f(math.subset(x, math.index(i, 0))))
    }
  }

  const mtm = math.multiply(math.transpose(m), m)
  const mty = math.multiply(math.transpose(m), y)
  let a = math.multiply(math.inv(mtm), mty)
  const vetorErro = math.subtract(math.multiply(m, a), y)
  const erroQuadratico = math.dot(vetorErro, vetorErro)

  if (nargs > n) {
    let saida = args[nargs - 1]
    if (saida < n) a = math.subset(a, math.index(saida, 0))
    else a = erroQuadratico
  }

  return a
}
