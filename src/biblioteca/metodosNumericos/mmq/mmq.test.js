import { mmq } from './mmq'
import { create, all } from 'mathjs'

const math = create(all)
math.import({
  mmq: mmq,
})

describe('Argumentos incorretos', () => {
  it('Número de funções incoerente', () => {
    try {
      math.evaluate('mmq([[1],[2],[3]], [[3],[4.1],[4.9]], 2, f(x)=3)')
    } catch (error) {
      expect(error.message).toContain('a quantidade de funções está incorreta.')
    }
  })
})

describe('Teste de saída de valores', () => {
  it('mmq vetor inteiro', () => {
    let resultadoParticularizacao = math.evaluate('mmq([[1],[2],[3]], [[3],[4.1],[4.9]],2,f(x)=1,f(x)=x)')
    resultadoParticularizacao = math.round(resultadoParticularizacao, 2)
    resultadoParticularizacao = math.evaluate('sum(abs(a-b))', { a: resultadoParticularizacao, b: [[2.1], [0.95]] })
    expect(resultadoParticularizacao).toEqual(0)
  })
  it('mmq elemento do vetor', () => {
    let resultadoParticularizacao = math.evaluate('mmq([[1],[2],[3]], [[3],[4.1],[4.9]],2,f(x)=1,f(x)=x,0)')
    resultadoParticularizacao = math.round(resultadoParticularizacao, 2)
    expect(resultadoParticularizacao).toEqual(2.1)
  })
  it('mmq erro quadrático', () => {
    let resultadoParticularizacao = math.evaluate('mmq([[1],[2],[3]], [[3],[4.1],[4.9]],2,f(x)=1,f(x)=x,2)')
    resultadoParticularizacao = math.round(resultadoParticularizacao, 3)
    expect(resultadoParticularizacao).toEqual(0.015)
  })
})
