import { validarEntradas } from '../../utils/validarEntradas'
import random from 'random'
import seedrandom from 'seedrandom'
import { create, all } from 'mathjs'
const math = create(all)

const validacao = {
  nomeDaFuncao: 'decomposicaoLu',
  requisitos: [
    {
      nome: 'semente',
      tipo: 'number',
      requerido: true,
    },
    {
      nome: 'ordem',
      tipo: 'number',
      requerido: true,
      condicoes: [
        {
          operador: '<=',
          valor: 10,
        },
      ],
    },
    {
      nome: 'saida',
      tipo: 'number',
      requerido: true,
    },
  ],
}

const gerarInteiro = (gerador) => {
  let numero
  do {
    numero = gerador.int(-5, 5)
  } while (numero >= -0.1 && numero <= 0.1)
  return numero
}

export const decomposicaoLu = (ordem, semente, saida = 0) => {
  const entradas = {
    ordem: ordem,
    semente: semente,
    saida: saida,
  }
  validarEntradas(entradas, validacao)

  const gerador = random.clone(seedrandom(semente))

  let L = math.identity(ordem)
  let U = math.zeros(ordem, ordem)
  let x = math.zeros(ordem, 1)
  for (let i = 0; i < ordem; i++) {
    for (let j = 0; j < ordem; j++) {
      if (i > j) L = math.subset(L, math.index(i, j), gerarInteiro(gerador))
      else U = math.subset(U, math.index(i, j), gerarInteiro(gerador))
    }
    x = math.subset(x, math.index(i, 0), gerarInteiro(gerador))
  }

  const A = math.multiply(L, U)
  const b = math.multiply(A, x)
  const somaX = math.sum(x)
  const somaLSomaU = math.sum(L) + math.sum(U)
  const somaLSomaX = math.sum(L) + math.sum(x)

  switch (saida) {
    case 0:
      return A
    case 1:
      return b
    case 2:
      return x
    case 3:
      return L
    case 4:
      return U
    case 5:
      return somaX
    case 6:
      return somaLSomaU
    case 7:
      return somaLSomaX
    default:
      return somaX
  }
}
