import { validarEntradas } from '../../utils/validarEntradas'
import { create, all } from 'mathjs'
const math = create(all)

const validacao = {
  nomeDaFuncao: 'gaussSeidel',
  requisitos: [
    {
      matrizA: 'A',
      matrizB: 'b',
      tipo: 'sistemaCompativel',
      requerido: true,
    },
    {
      matrizA: 'b',
      matrizB: 'x0',
      tipo: 'matrizesMesmaDimensao',
      requerido: true,
    },
    {
      nome: 'precisao',
      tipo: 'number',
      requerido: true,      
    },
    {
      nome: 'numMaxIt',
      tipo: 'number',
      requerido: true,
      condicoes: [
        {
          operador: '<=',
          valor: 50,
        },
      ],      
    },
    {
      nome: 'saida',
      tipo: 'number',
      requerido: false,      
    },
  ],
}


export const gaussSeidel = (A, b, x0, numMaxIt, precisao, saida = undefined) => {
  const entradas = {
    A: A,
    b: b,
    x0: x0,
    numMaxIt: numMaxIt,
    precisao: precisao,
    saida: saida,
  }
  validarEntradas(entradas, validacao)

  A = math.matrix(A)
  b = math.matrix(b)
  x0 = math.matrix(x0)
  const dimensao = math.size(A)
  const ordem = math.subset(dimensao, math.index(0))

  const B = math.evaluate('identity(size(A)) - inv(A .* identity(size(A))) * A', { A: A })
  const g = math.evaluate('inv(A .* identity(size(A))) * b', { A: A, b: b })

  let x = x0
  let xOld
  let xi
  let erro = 100
  let nIteracao = 0
  while (erro > math.pow(10, -precisao) && nIteracao < numMaxIt) {
    xOld = x
    for (let i = 1; i <= ordem; i++) {
      xi = math.evaluate('B[i,:]*x + g[i,1]', { B: B, g: g, x: x, i: i })
      x = math.subset(x, math.index(i - 1, 0), math.subset(xi, math.index(0, 0)))
    }
    erro = math.evaluate('max(abs(x-xOld))/max(abs(x))', { x: x, xOld: xOld })
    nIteracao++
  }

  if (saida >= 0 && saida < ordem) return math.subset(x, math.index(saida, 0))
  else if (saida === ordem) return nIteracao
  else if (saida === ordem + 1) return erro
  else if (saida === ordem + 2) return math.sum(x)
  else return x
}
