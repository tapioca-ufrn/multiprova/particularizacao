import { jacobiRichardson } from './jacobiRichardson'
import { create, all } from 'mathjs'

const math = create(all)
math.import({
  jacobiRichardson: jacobiRichardson,
})

describe('Teste de saída de valores', () => {
  it('Raiz encontrada. Parada pelo número de iterações. Saída Elemento 1', () => {
    let resultadoParticularizacao = math.evaluate('jacobiRichardson([[a,2],[1,5]], [[6],[4]], [[0],[0]], 2, 7, 0)', {
      a: 4,
    })
    expect(math.round(resultadoParticularizacao, 1)).toEqual(1.1)
  })
  it('Raiz encontrada. Parada pelo número de iterações. Saída Elemento 2', () => {
    let resultadoParticularizacao = math.evaluate('jacobiRichardson([[4,2],[1,5]], [[6],[4]], [[0],[0]], 2, 7, 1)')
    expect(math.round(resultadoParticularizacao, 1)).toEqual(0.5)
  })
  it('Raiz encontrada. Parada pelo número de iterações. Saída Raíz', () => {
    let resultadoParticularizacao = math.evaluate('jacobiRichardson([[4,2],[1,5]], [[6],[4]], [[0],[0]], 2, 7)')
    resultadoParticularizacao = math.sum(resultadoParticularizacao)
    expect(math.round(resultadoParticularizacao, 1)).toEqual(1.6)
  })

  it('Raiz encontrada. Parada pelo erro de iterações. Saída numero iteracao', () => {
    let resultadoParticularizacao = math.evaluate('jacobiRichardson([[4,2],[1,5]], [[6],[4]], [[0],[0]], 40, 4, 2)')
    expect(resultadoParticularizacao).toEqual(10)
  })

  it('Raiz encontrada. Parada pelo número de iterações. Saída erro ', () => {
    let resultadoParticularizacao = math.evaluate('jacobiRichardson([[4,2],[1,5]], [[6],[4]], [[0],[0]], 7, 4, 3)')
    const resultadoArredondado = math.round(resultadoParticularizacao, 6)
    expect(resultadoArredondado).toEqual(0.001227)
  })
  it('Raiz encontrada. Parada pelo número de iterações. Saída soma dos valores de x ', () => {
    let resultadoParticularizacao = math.evaluate('jacobiRichardson([[4,2],[1,5]], [[6],[4]], [[0],[0]], 4, 7, 4)')
    expect(resultadoParticularizacao).toEqual(1.76)
  })
})
