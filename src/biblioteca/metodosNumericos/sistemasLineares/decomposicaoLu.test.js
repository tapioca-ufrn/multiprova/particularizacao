import { decomposicaoLu } from './decomposicaoLu'
import { create, all } from 'mathjs'

const math = create(all)
math.import({
  decomposicaoLu: decomposicaoLu,
})

describe('Teste de saída de valores', () => {
  it('Saida A', () => {
    let resultadoParticularizacao = math.evaluate('decomposicaoLu(3, 34)')
    expect(math.sum(resultadoParticularizacao)).toEqual(52)
  })
  it('Saida b', () => {
    let resultadoParticularizacao = math.evaluate('decomposicaoLu(3, 34, 1)')
    expect(math.sum(resultadoParticularizacao)).toEqual(-153)
  })
  it('Saida x', () => {
    let resultadoParticularizacao = math.evaluate('decomposicaoLu(3, 34, 2)')
    expect(math.sum(resultadoParticularizacao)).toEqual(-5)
  })
  it('Saida L', () => {
    let resultadoParticularizacao = math.evaluate('decomposicaoLu(3, 34, 3)')
    expect(math.sum(resultadoParticularizacao)).toEqual(7)
  })
  it('Saida U', () => {
    let resultadoParticularizacao = math.evaluate('decomposicaoLu(3, 34, 4)')
    expect(math.sum(resultadoParticularizacao)).toEqual(12)
  })
  it('Saida somaX', () => {
    let resultadoParticularizacao = math.evaluate('decomposicaoLu(3, 34, 5)')
    expect(resultadoParticularizacao).toEqual(-5)
  })
  it('Saida somaLSomaU', () => {
    let resultadoParticularizacao = math.evaluate('decomposicaoLu(3, 34, 6)')
    expect(resultadoParticularizacao).toEqual(19)
  })
  it('Saida somaLSomaX', () => {
    let resultadoParticularizacao = math.evaluate('decomposicaoLu(3, 34, 7)')
    expect(resultadoParticularizacao).toEqual(2)
  })
  it('Saida somaX', () => {
    let resultadoParticularizacao = math.evaluate('decomposicaoLu(3, 34, 10)')
    expect(resultadoParticularizacao).toEqual(-5)
  })
})
