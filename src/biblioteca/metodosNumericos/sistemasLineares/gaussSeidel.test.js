import { gaussSeidel } from './gaussSeidel'
import { create, all } from 'mathjs'

const math = create(all)
math.import({
  gaussSeidel: gaussSeidel,
})

describe('Teste de saída de valores', () => {
  it('Raiz encontrada. Parada pelo número de iterações. Saída Raíz', () => {
    let resultadoParticularizacao = math.evaluate('gaussSeidel([[4,2],[1,5]], [[6],[4]], [[0],[0]], 4, 7)')
    resultadoParticularizacao = math.sum(resultadoParticularizacao)
    expect(math.round(resultadoParticularizacao, 3)).toEqual(1.778)
  })
  it('Raiz encontrada. Parada pelo número de iterações. Saída x[0]', () => {
    let resultadoParticularizacao = math.evaluate('gaussSeidel([[4,2],[1,5]], [[6],[4]], [[0],[0]], 4, 7, 0)')
    expect(math.round(resultadoParticularizacao, 3)).toEqual(1.223)
  })
  it('Raiz encontrada. Parada pelo número de iterações. Saída x[1]', () => {
    let resultadoParticularizacao = math.evaluate('gaussSeidel([[4,2],[1,5]], [[6],[4]], [[0],[0]], 4, 7, 1)')
    expect(math.round(resultadoParticularizacao, 3)).toEqual(0.556)
  })
  it('Raiz encontrada. Parada pelo erro de iterações. Saída numero iteracao', () => {
    let resultadoParticularizacao = math.evaluate('gaussSeidel([[4,2],[1,5]], [[6],[4]], [[0],[0]], 40, 4, 2)')
    expect(resultadoParticularizacao).toEqual(6)
  })
  it('Raiz encontrada. Parada pelo número de iterações. Saída erro ', () => {
    let resultadoParticularizacao = math.evaluate('gaussSeidel([[4,2],[1,5]], [[6],[4]], [[0],[0]], 4, 4, 3)')
    const resultadoArredondado = math.round(resultadoParticularizacao, 6)
    expect(resultadoArredondado).toEqual(0.002045)
  })
  it('Raiz encontrada. Parada pelo número de iterações. Saída sum(x) ', () => {
    let resultadoParticularizacao = math.evaluate('gaussSeidel([[4,2],[1,5]], [[6],[4]], [[0],[0]], 4, 4, 4)')
    const resultadoArredondado = math.round(resultadoParticularizacao, 3)
    expect(resultadoArredondado).toEqual(1.778)
  })
})
