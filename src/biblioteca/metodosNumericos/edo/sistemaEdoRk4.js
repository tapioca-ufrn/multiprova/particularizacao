import { validarEntradas } from '../../utils/validarEntradas'
import { create, all } from 'mathjs'
const math = create(all)

const ajustarValidacao = () => {
  return {
    nomeDaFuncao: 'edoEuler',
    requisitos: [
      {
        nome: 'f1',
        tipo: 'Function',
        requerido: true,
      },
      {
        nome: 'f2',
        tipo: 'Function',
        requerido: true,
      },
      {
        nome: 'x0',
        tipo: 'number',
        requerido: true,
      },
      {
        nome: 'y0',
        tipo: 'number',
        requerido: true,
      },
      {
        nome: 'z0',
        tipo: 'number',
        requerido: true,
      },
      {
        nome: 'h',
        tipo: 'number',
        requerido: true,
      },
      {
        nome: 'n',
        tipo: 'number',
        requerido: true,
      },
      {
        nome: 'saida',
        tipo: 'number',
        requerido: true,
      },
    ],
  }
}

export const sistemaEdoRk4 = (f1, f2, x0, y0, z0, h, n, saida = 0) => {
  const entradas = {
    f1: f1,
    f2: f2,
    x0: x0,
    y0: y0,
    z0: z0,
    h: h,
    n: n,
    saida: saida,
  }
  const validacao = ajustarValidacao()
  validarEntradas(entradas, validacao)
  n = math.round(n)

  let y = y0
  let z = z0
  let x = x0
  let k11, k12, k21, k22, k31, k32, k41, k42
  let somaK = 0
  for (let i = 1; i <= n; i++) {
    k11 = f1(x, y, z)
    k12 = f2(x, y, z)
    k21 = f1(x + h / 2, y + (k11 * h) / 2, z + (k12 * h) / 2)
    k22 = f2(x + h / 2, y + (k11 * h) / 2, z + (k12 * h) / 2)
    k31 = f1(x + h / 2, y + (k21 * h) / 2, z + (k22 * h) / 2)
    k32 = f2(x + h / 2, y + (k21 * h) / 2, z + (k22 * h) / 2)
    k41 = f1(x + h, y + k31 * h, z + k32 * h)
    k42 = f2(x + h, y + k31 * h, z + k32 * h)
    y = y + (1 / 6) * (k11 + 2 * k21 + 2 * k31 + k41) * h
    z = z + (1 / 6) * (k12 + 2 * k22 + 2 * k32 + k42) * h
    x = x + h
    somaK = somaK + k42
  }

  switch (saida) {
    case 1:
      return y
    case 2:
      return z
    case 3:
      return somaK
    default:
      return [[y], [z]]
  }
}
