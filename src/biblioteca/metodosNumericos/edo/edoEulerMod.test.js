import { edoEulerMod } from './edoEulerMod'
import { create, all } from 'mathjs'

const math = create(all)
math.import({
  edoEulerMod: edoEulerMod,
})

describe('Teste de saída de valores', () => {
  it('EDO - Método de Euler Modificado', () => {
    let resultadoParticularizacao = math.evaluate('edoEulerMod(f(x,y)=x^2*cos(x),0,1,0.01,100)')
    resultadoParticularizacao = math.round(resultadoParticularizacao, 7)
    expect(resultadoParticularizacao).toEqual(1.2391356)
  })
  it('EDO - Método de Euler Modificado - saida soma dos k2', () => {
    let resultadoParticularizacao = math.evaluate('edoEulerMod(f(x,y)=x^2*cos(x),0,1,0.01,100,1)')
    resultadoParticularizacao = math.round(resultadoParticularizacao, 3)
    expect(resultadoParticularizacao).toEqual(24.184)
  })
})
