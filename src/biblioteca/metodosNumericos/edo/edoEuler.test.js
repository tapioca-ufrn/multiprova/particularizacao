import { edoEuler } from './edoEuler'
import { create, all } from 'mathjs'

const math = create(all)
math.import({
  edoEuler: edoEuler,
})

describe('Teste de saída de valores', () => {
  it('EDO - Método de Euler', () => {
    let resultadoParticularizacao = math.evaluate('edoEuler(f(x,y)=x^2*cos(x),0,1,0.01,100)')
    resultadoParticularizacao = math.round(resultadoParticularizacao, 5)
    expect(resultadoParticularizacao).toEqual(1.23643)
  })
  it('EDO - Método de Euler - saída soma de k', () => {
    let resultadoParticularizacao = math.evaluate('edoEuler(f(x,y)=x^2*cos(x),0,1,0.01,100,1)')
    resultadoParticularizacao = math.round(resultadoParticularizacao, 3)
    expect(resultadoParticularizacao).toEqual(23.643)
  })
})
