import { sistemaEdoPontoMedio } from './sistemaEdoPontoMedio'
import { create, all } from 'mathjs'

const math = create(all)
math.import({
  sistemaEdoPontoMedio: sistemaEdoPontoMedio,
})

describe('Teste de saída de valores', () => {
  it('Sistema EDO - Método do Ponto Médio - retorna y e z', () => {
    let resultadoParticularizacao = math.evaluate('sistemaEdoPontoMedio(f(x,y,z)=x*z+y,g(x,y,z)=z+x*y,2,3,-2,0.5,3)')
    resultadoParticularizacao = math.round(math.sum(resultadoParticularizacao), 6)
    expect(resultadoParticularizacao).toEqual(91.123901)
  })
  it('Sistema EDO - Método do Ponto Médio - retorna y', () => {
    let resultadoParticularizacao = math.evaluate('sistemaEdoPontoMedio(f(x,y,z)=x*z+y,g(x,y,z)=z+x*y,2,3,-2,0.5,3,1)')
    resultadoParticularizacao = math.round(resultadoParticularizacao, 5)
    expect(resultadoParticularizacao).toEqual(45.82524)
  })
  it('Sistema EDO - Método do Ponto Médio - retorna z', () => {
    let resultadoParticularizacao = math.evaluate('sistemaEdoPontoMedio(f(x,y,z)=x*z+y,g(x,y,z)=z+x*y,2,3,-2,0.5,3,2)')
    resultadoParticularizacao = math.round(resultadoParticularizacao, 5)
    expect(resultadoParticularizacao).toEqual(45.29866)
  })
  it('Sistema EDO - Método do Ponto Médio - saída soma de k', () => {
    let resultadoParticularizacao = math.evaluate('sistemaEdoPontoMedio(f(x,y,z)=x*z+y,g(x,y,z)=z+x*y,2,3,-2,0.5,3,3)')
    resultadoParticularizacao = math.round(resultadoParticularizacao, 3)
    expect(resultadoParticularizacao).toEqual(94.597)
  })
})
