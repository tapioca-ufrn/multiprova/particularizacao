import { edoRungeKutta4 } from './edoRungeKutta4'
import { create, all } from 'mathjs'

const math = create(all)
math.import({
  edoRungeKutta4: edoRungeKutta4,
})

describe('Teste de saída de valores', () => {
  it('EDO - Método de RungeKutta ordem 4', () => {
    let resultadoParticularizacao = math.evaluate('edoRungeKutta4(f(x,y)=x^2*cos(x),0,1,0.01,100)')
    resultadoParticularizacao = math.round(resultadoParticularizacao, 7)
    expect(resultadoParticularizacao).toEqual(1.2391336)
  })
  it('EDO - Método de RungeKutta ordem 4 - Saída soma k4', () => {
    let resultadoParticularizacao = math.evaluate('edoRungeKutta4(f(x,y)=x^2*cos(x),0,1,0.01,100,1)')
    resultadoParticularizacao = math.round(resultadoParticularizacao, 3)
    expect(resultadoParticularizacao).toEqual(24.184)
  })
})
