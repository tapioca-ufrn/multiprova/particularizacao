import { sistemaEdoEulerMod } from './sistemaEdoEulerMod'
import { create, all } from 'mathjs'

const math = create(all)
math.import({
  sistemaEdoEulerMod: sistemaEdoEulerMod,
})

describe('Teste de saída de valores', () => {
  it('Sistema EDO - Método de Euler Modificado - retorna y e z', () => {
    let resultadoParticularizacao = math.evaluate('sistemaEdoEulerMod(f(x,y,z)=x*z+y,g(x,y,z)=z+x*y,2,3,-2,0.5,3)')
    resultadoParticularizacao = math.round(math.sum(resultadoParticularizacao), 6)
    expect(resultadoParticularizacao).toEqual(97.883789)
  })
  it('Sistema EDO - Método de Euler Modificado - retorna y', () => {
    let resultadoParticularizacao = math.evaluate('sistemaEdoEulerMod(f(x,y,z)=x*z+y,g(x,y,z)=z+x*y,2,3,-2,0.5,3,1)')
    resultadoParticularizacao = math.round(resultadoParticularizacao, 5)
    expect(resultadoParticularizacao).toEqual(49.29346)
  })
  it('Sistema EDO - Método de Euler Modificado - retorna z', () => {
    let resultadoParticularizacao = math.evaluate('sistemaEdoEulerMod(f(x,y,z)=x*z+y,g(x,y,z)=z+x*y,2,3,-2,0.5,3,2)')
    resultadoParticularizacao = math.round(resultadoParticularizacao, 5)
    expect(resultadoParticularizacao).toEqual(48.59033)
  })
  it('Sistema EDO - Método de Euler Modificado - saída soma de k', () => {
    let resultadoParticularizacao = math.evaluate('sistemaEdoEulerMod(f(x,y,z)=x*z+y,g(x,y,z)=z+x*y,2,3,-2,0.5,3,3)')
    resultadoParticularizacao = math.round(resultadoParticularizacao, 3)
    expect(resultadoParticularizacao).toEqual(151.533)
  })
})
