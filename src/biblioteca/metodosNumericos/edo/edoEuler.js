import { validarEntradas } from '../../utils/validarEntradas'
import { create, all } from 'mathjs'
const math = create(all)

const ajustarValidacao = () => {
  return {
    nomeDaFuncao: 'edoEuler',
    requisitos: [
      {
        nome: 'f',
        tipo: 'Function',
        requerido: true,
      },
      {
        nome: 'x0',
        tipo: 'number',
        requerido: true,
      },
      {
        nome: 'y0',
        tipo: 'number',
        requerido: true,
      },
      {
        nome: 'h',
        tipo: 'number',
        requerido: true,
      },
      {
        nome: 'n',
        tipo: 'number',
        requerido: true,
      },
      {
        nome: 'saida',
        tipo: 'number',
        requerido: true,
      },
    ],
  }
}

export const edoEuler = (f, x0, y0, h, n, saida = 0) => {
  const entradas = {
    f: f,
    x0: x0,
    y0: y0,
    h: h,
    n: n,
    saida: saida,
  }
  const validacao = ajustarValidacao()
  validarEntradas(entradas, validacao)
  n = math.round(n)

  let y = y0
  let x = x0
  let k
  let somaK = 0
  for (let i = 1; i <= n; i++) {
    k = f(x, y)
    y = y + k * h
    x = x + h
    somaK = somaK + k
  }

  switch (saida) {
    case 1:
      return somaK
    default:
      return y
  }
}
