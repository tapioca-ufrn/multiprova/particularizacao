import { edoPontoMedio } from './edoPontoMedio'
import { create, all } from 'mathjs'

const math = create(all)
math.import({
  edoPontoMedio: edoPontoMedio,
})

describe('Teste de saída de valores', () => {
  it('EDO - Método do ponto médio', () => {
    let resultadoParticularizacao = math.evaluate('edoPontoMedio(f(x,y)=x^2*cos(x),0,1,0.01,100)')
    resultadoParticularizacao = math.round(resultadoParticularizacao, 7)
    expect(resultadoParticularizacao).toEqual(1.2391326)
  })
  it('EDO - Método do ponto médio - saida soma dos k2', () => {
    let resultadoParticularizacao = math.evaluate('edoPontoMedio(f(x,y)=x^2*cos(x),0,1,0.01,100,1)')
    resultadoParticularizacao = math.round(resultadoParticularizacao, 7)
    expect(resultadoParticularizacao).toEqual(23.913263)
  })
})
