import { validarEntradas } from '../../utils/validarEntradas'
import { create, all } from 'mathjs'
const math = create(all)

const ajustarValidacao = () => {
  return {
    nomeDaFuncao: 'edoRk4',
    requisitos: [
      {
        nome: 'f',
        tipo: 'Function',
        requerido: true,
      },
      {
        nome: 'x0',
        tipo: 'number',
        requerido: true,
      },
      {
        nome: 'y0',
        tipo: 'number',
        requerido: true,
      },
      {
        nome: 'h',
        tipo: 'number',
        requerido: true,
      },
      {
        nome: 'n',
        tipo: 'number',
        requerido: true,
      },
      {
        nome: 'saida',
        tipo: 'number',
        requerido: true,
      },
    ],
  }
}

export const edoRungeKutta4 = (f, x0, y0, h, n, saida = 0) => {
  const entradas = {
    f: f,
    x0: x0,
    y0: y0,
    h: h,
    n: n,
    saida: saida,
  }
  const validacao = ajustarValidacao()
  validarEntradas(entradas, validacao)
  n = math.round(n)

  let y = y0
  let x = x0
  let k1, k2, k3, k4
  let somaK = 0
  for (let i = 1; i <= n; i++) {
    k1 = f(x, y)
    k2 = f(x + h / 2, y + (k1 * h) / 2)
    k3 = f(x + h / 2, y + (k2 * h) / 2)
    k4 = f(x + h, y + k3 * h)
    y = y + ((k1 + 2 * k2 + 2 * k3 + k4) / 6) * h
    x = x + h
    somaK = somaK + k4
  }

  switch (saida) {
    case 1:
      return somaK
    default:
      return y
  }
}
