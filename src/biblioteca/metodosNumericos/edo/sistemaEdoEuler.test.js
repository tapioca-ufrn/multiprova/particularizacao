import { sistemaEdoEuler } from './sistemaEdoEuler'
import { create, all } from 'mathjs'

const math = create(all)
math.import({
  sistemaEdoEuler: sistemaEdoEuler,
})

describe('Teste de saída de valores', () => {
  it('Sistema EDO - Método de Euler - retorna y e z', () => {
    let resultadoParticularizacao = math.evaluate('sistemaEdoEuler(f(x,y,z)=y+z,g(x,y,z)=y+x,0,0,0,1,3)')
    resultadoParticularizacao = math.round(math.sum(resultadoParticularizacao), 0)
    expect(resultadoParticularizacao).toEqual(4)
  })
  it('Sistema EDO - Método de Euler - retorna y', () => {
    let resultadoParticularizacao = math.evaluate('sistemaEdoEuler(f(x,y,z)=y+z,g(x,y,z)=y+x,0,0,0,1,3,1)')
    resultadoParticularizacao = math.round(resultadoParticularizacao)
    expect(resultadoParticularizacao).toEqual(1)
  })
  it('Sistema EDO - Método de Euler - retorna z', () => {
    let resultadoParticularizacao = math.evaluate('sistemaEdoEuler(f(x,y,z)=y+z,g(x,y,z)=y+x,0,0,0,1,3,2)')
    resultadoParticularizacao = math.round(resultadoParticularizacao)
    expect(resultadoParticularizacao).toEqual(3)
  })
  it('Sistema EDO - Método de Euler - saída soma de k', () => {
    let resultadoParticularizacao = math.evaluate('sistemaEdoEuler(f(x,y,z)=y+z,g(x,y,z)=y+x,0,0,0,1,3,3)')
    resultadoParticularizacao = math.round(resultadoParticularizacao, 3)
    expect(resultadoParticularizacao).toEqual(1)
  })
})
