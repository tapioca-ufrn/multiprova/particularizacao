import { sistemaEdoRk4 } from './sistemaEdoRk4'
import { create, all } from 'mathjs'

const math = create(all)
math.import({
  sistemaEdoRk4: sistemaEdoRk4,
})

describe('Teste de saída de valores', () => {
  it('Sistema EDO - Método RK4 - retorna y e z', () => {
    let resultadoParticularizacao = math.evaluate('sistemaEdoRk4(f(x,y,z)=x*z+y,g(x,y,z)=z+x*y,2,3,-2,0.5,3)')
    resultadoParticularizacao = math.round(math.sum(resultadoParticularizacao), 6)
    expect(resultadoParticularizacao).toEqual(241.941481)
  })
  it('Sistema EDO - Método RK4- retorna y', () => {
    let resultadoParticularizacao = math.evaluate('sistemaEdoRk4(f(x,y,z)=x*z+y,g(x,y,z)=z+x*y,2,3,-2,0.5,3,1)')
    resultadoParticularizacao = math.round(resultadoParticularizacao, 5)
    expect(resultadoParticularizacao).toEqual(121.16002)
  })
  it('Sistema EDO - Método RK4 - retorna z', () => {
    let resultadoParticularizacao = math.evaluate('sistemaEdoRk4(f(x,y,z)=x*z+y,g(x,y,z)=z+x*y,2,3,-2,0.5,3,2)')
    resultadoParticularizacao = math.round(resultadoParticularizacao, 5)
    expect(resultadoParticularizacao).toEqual(120.78146)
  })
  it('Sistema EDO - Método RK4 - saída soma de k', () => {
    let resultadoParticularizacao = math.evaluate('sistemaEdoRk4(f(x,y,z)=x*z+y,g(x,y,z)=z+x*y,2,3,-2,0.5,3,3)')
    resultadoParticularizacao = math.round(resultadoParticularizacao, 3)
    expect(resultadoParticularizacao).toEqual(604.206)
  })
})
