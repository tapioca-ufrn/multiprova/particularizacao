import { create, all } from 'mathjs'
const math = create(all)

export const validarNumero = (funcao, requisito, valor = null) => {
  if (requisito.requerido === true) {
    if (valor == null) throw new Error(`Função ${funcao}(): o parâmetro '${requisito.nome}' é obrigatório.`)
  }
  if ((math.typeOf(valor) !== 'number') & (math.typeOf(valor) != 'null')) {
    throw new Error(`Função ${funcao}(): o parâmetro '${requisito.nome}' deve ser um número.`)
  }
  if (requisito.condicoes != null) {
    if ((requisito.condicoes.length > 0) & (math.typeOf(valor) != 'null')) {
      for (const condicao of requisito.condicoes) {
        const stringCondicao = `${valor} ${condicao.operador} ${condicao.valor} `
        if (math.evaluate(stringCondicao) !== true) {
          throw new Error(
            `Função ${funcao}(): o parâmetro '${requisito.nome}' deve obedecer a condição '${requisito.nome} ${condicao.operador} ${condicao.valor}'.`,
          )
        }
      }
    }
  }

  return true
}
