import { create, all } from 'mathjs'
const math = create(all)

export const validarFuncao = (funcao, requisito, valor = null) => {
  if (requisito.requerido === true) {
    if (valor == null) throw new Error(`Função ${funcao}(): o parâmetro '${requisito.nome}' é uma função obrigatória.`)
  }
  if ((math.typeOf(valor) !== 'Function') & (math.typeOf(valor) != 'null')) {
    throw new Error(
      `Função ${funcao}(): o parâmetro '${requisito.nome}' deve ser uma função. Atualmente é do tipo: ${math.typeOf(
        valor,
      )}.`,
    )
  }
  return true
}
