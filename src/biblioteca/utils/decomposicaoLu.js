import { create, all } from 'mathjs'
const math = create(all)

const obtemOrdem = (A) => {
  const dimensao = math.size(A)
  return math.subset(dimensao, math.index(0))
}

const temDecomposicaoLU = (A) => {
  const ordem = obtemOrdem(A)

  for (let i = 1; i < ordem; i++) {
    const range = math.range(0, i)
    const menorPrincipal = math.subset(A, math.index(range, range))
    if (math.det(menorPrincipal) === 0) return false
  }
  return true
}

const validaMatriz = (A) => {
  if (!temDecomposicaoLU(A)) {
    throw new Error('Matriz não possui decomposição LU')
  }
}

export const decomposicaoLu = (A) => {
  A = math.matrix(A)
  validaMatriz(A)

  const ordem = obtemOrdem(A)
  let L = math.identity(math.size(A))

  for (let j = 1; j <= ordem - 1; j++) {
    const pivo = math.subset(A, math.index(j - 1, j - 1))
    for (let i = j + 1; i <= ordem; i++) {
      const fator = math.subset(A, math.index(i - 1, j - 1)) / pivo
      const linhaI = math.evaluate('A[i,:] - f*A[j,:]', { A: A, i: i, j: j, f: fator })
      A = math.subset(A, math.index(i - 1, math.range(0, ordem)), linhaI)
      L = math.subset(L, math.index(i - 1, j - 1), fator)
    }
  }

  return { L: L, U: A }
}
