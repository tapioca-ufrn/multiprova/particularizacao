import { create, all } from 'mathjs'
const math = create(all)

export const validarVetorColuna = (funcao, requisito, v = null) => {
  const tipos = ['Array', 'Matrix']

  if (v == null) {
    if (requisito.requerido === true) {
      throw new Error(`Função ${funcao}(): o vetor coluna '${requisito.nome}' é obrigatório.`)
    } else return true
  }

  if (!tipos.includes(math.typeOf(v))) {
    throw new Error(`Função ${funcao}(): o parâmetro '${requisito.nome}' deve ser um matriz (vetor coluna).`)
  }

  let ehVetorColuna = true
  const dimV = math.typeOf(v) === 'Matrix' ? v.size() : math.size(v)
  if (dimV.length === 2) {
    if (dimV[1] !== 1) ehVetorColuna = false
  } else ehVetorColuna = false

  if (ehVetorColuna === false) {
    throw new Error(`Função ${funcao}(): a matriz '${requisito.nome}' deve estar no formato de um vetor coluna.`)
  }

  return true
}
