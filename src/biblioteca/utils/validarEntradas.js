import { validarNumero } from './validarNumero'
import { validarFuncao } from './validarFuncao'
import { validarMatriz } from './validarMatriz'
import { validarMatrizesMesmaDim } from './validarMatrizesMesmaDim'
import { validarSistemaCompativel } from './validarSistemaCompativel'
import { validarVetorColuna } from './validarVetorColuna'
import { validarVetorLinha } from './validarVetorLinha'

export function validarEntradas(entradas, validacao) {
  const funcao = validacao.nomeDaFuncao
  const requisitos = validacao.requisitos

  for (const requisito of requisitos) {
    switch (requisito.tipo) {
      case 'number':
        validarNumero(funcao, requisito, entradas[requisito.nome])
        break
      case 'Function':
        validarFuncao(funcao, requisito, entradas[requisito.nome])
        break
      case 'Matrix':
        validarMatriz(funcao, requisito, entradas[requisito.nome])
        break
      case 'sistemaCompativel':
        validarSistemaCompativel(funcao, requisito, entradas[requisito.matrizA], entradas[requisito.matrizB])
        break
      case 'matrizesMesmaDimensao':
        validarMatrizesMesmaDim(funcao, requisito, entradas[requisito.matrizA], entradas[requisito.matrizB])
        break
      case 'vetorColuna':
        validarVetorColuna(funcao, requisito, entradas[requisito.nome])
        break
      case 'vetorLinha':
        validarVetorLinha(funcao, requisito, entradas[requisito.nome])
        break
    }
  }
  return true
}
