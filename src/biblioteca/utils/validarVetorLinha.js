import { create, all } from 'mathjs'
const math = create(all)

export const validarVetorLinha = (funcao, requisito, v = null) => {
  const tipos = ['Array', 'Matrix']

  if (v == null) {
    if (requisito.requerido === true) {
      throw new Error(`Função ${funcao}(): o vetor linha '${requisito.nome}' é obrigatório.`)
    } else return true
  }

  if (!tipos.includes(math.typeOf(v))) {
    throw new Error(`Função ${funcao}(): o parâmetro '${requisito.nome}' deve ser um matriz (vetor linha).`)
  }

  let ehVetorLinha = true
  const dimV = math.typeOf(v) === 'Matrix' ? v.size() : math.size(v)
  if (dimV.length === 2) {
    if (dimV[0] !== 1) ehVetorLinha = false
  } else ehVetorLinha = false

  if (ehVetorLinha === false) {
    throw new Error(`Função ${funcao}(): a matriz '${requisito.nome}' deve estar no formato de um vetor linha.`)
  }

  return true
}
