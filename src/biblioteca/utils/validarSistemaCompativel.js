import { create, all } from 'mathjs'
const math = create(all)

export const validarSistemaCompativel = (funcao, requisito, A = null, b = null) => {
  const tipos = ['Array', 'Matrix']

  if (requisito.requerido === true) {
    if (A == null || b == null)
      throw new Error(
        `Função ${funcao}(): as matrizes '${requisito.matrizA}' e '${requisito.matrizB}' são obrigatórias.`,
      )
  } else {
    if (A == null && b == null) return true
  }

  if (!(tipos.includes(math.typeOf(A)) && tipos.includes(math.typeOf(b)))) {
    throw new Error(
      `Função ${funcao}(): os parâmetros '${requisito.matrizA}' e '${requisito.matrizB}' devem ser matrizes.`,
    )
  }

  let sistemaIncompativel = true
  const dimA = math.typeOf(A) === 'Matrix' ? A.size() : math.size(A)
  const dimB = math.typeOf(b) === 'Matrix' ? b.size() : math.size(b)
  if (dimA[1] === dimB[0] && dimB[1] === 1 && dimA[0] === dimA[1]) sistemaIncompativel = false
  if (sistemaIncompativel) {
    throw new Error(
      `Função ${funcao}(): as matrizes '${requisito.matrizA}' e '${requisito.matrizB}' devem ter dimensões compatíveis.`,
    )
  }

  return true
}
