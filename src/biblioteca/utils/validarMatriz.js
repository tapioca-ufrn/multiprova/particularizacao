import { create, all } from 'mathjs'
const math = create(all)

const tipos = ['Array', 'Matrix']

export const validarMatriz = (funcao, requisito, valor = null) => {
  if (requisito.requerido === true) {
    if (valor == null) throw new Error(`Função ${funcao}(): o parâmetro '${requisito.nome}' é uma matriz obrigatória.`)
  }
  if (!tipos.includes(math.typeOf(valor)) & (math.typeOf(valor) != 'null')) {
    throw new Error(
      `Função ${funcao}(): o parâmetro '${requisito.nome}' deve ser uma matriz. Atualmente é do tipo: ${math.typeOf(
        valor,
      )}.`,
    )
  }
  return true
}
