import { create, all } from 'mathjs'
const math = create(all)

export const validarMatrizesMesmaDim = (funcao, requisito, A = null, B = null) => {
  const tipos = ['Array', 'Matrix']
  if (requisito.requerido === true) {
    if (A == null || B == null)
      throw new Error(
        `Função ${funcao}(): as matrizes '${requisito.matrizA}' e '${requisito.matrizB}' são obrigatórias.`,
      )
  } else {
    if (A == null && B == null) return true
  }

  if (!(tipos.includes(math.typeOf(A)) && tipos.includes(math.typeOf(B)))) {
    throw new Error(
      `Função ${funcao}(): os parâmetros '${requisito.matrizA}' e '${requisito.matrizB}' devem ser matrizes.`,
    )
  }

  let naoTemMesmaDim = true
  const dimA = math.typeOf(A) === 'Matrix' ? A.size() : math.size(A)
  const dimB = math.typeOf(B) === 'Matrix' ? B.size() : math.size(B)
  const teste = math.evaluate('sum(abs(dimA-dimB))', { dimA: dimA, dimB: dimB })
  if (teste === 0) naoTemMesmaDim = false
  if (naoTemMesmaDim) {
    throw new Error(
      `Função ${funcao}(): as matrizes '${requisito.matrizA}' e '${requisito.matrizB}' devem ter a mesma dimensão.`,
    )
  }

  return true
}
