import { validarFuncao } from '../validarFuncao'

describe('Validar Numero', () => {
  it('Parâmetro f deve ser obrigatório', () => {
    const requisito = {
      nome: 'f',
      tipo: 'Function',
      requerido: true,
    }
    try {
      validarFuncao('testJest', requisito)
    } catch (e) {
      expect(e.message).toEqual(`Função testJest(): o parâmetro 'f' é uma função obrigatória.`)
    }
  })
  it('Parâmetro a deve ser uma função', () => {
    const requisito = {
      nome: 'f',
      tipo: 'Function',
      requerido: true,
    }
    try {
      validarFuncao('testJest', requisito, 2)
    } catch (e) {
      expect(e.message).toEqual(`Função testJest(): o parâmetro 'f' deve ser uma função. Atualmente é do tipo: number.`)
    }
  })
  it('f é uma função. Validação passa', () => {
    const requisito = {
      nome: 'f',
      tipo: 'Function',
      requerido: false,
    }
    const resultado = validarFuncao('testJest', requisito, function () {})
    expect(resultado).toEqual(true)
  })
})
