import { validarEntradas } from '../validarEntradas'

describe('Validar Numero', function teste() {
  it('Faz a decomposição corretamente', function testesJest() {
    const validacao = {
      nomeDaFuncao: 'testeJest',
      requisitos: [
        {
          nome: 'a',
          tipo: 'number',
          requerido: false,
        },
        {
          nome: 'f',
          tipo: 'Function',
          requerido: false,
        },
        {
          nome: 'M',
          tipo: 'Matrix',
          requerido: false,
        },
        {
          tipo: 'sistemaCompativel',
          matrizA: 'A',
          matrizB: 'b',
          requerido: false,
        },
        {
          tipo: 'matrizesMesmaDimensao',
          matrizA: 'A',
          matrizB: 'B',
          requerido: false,
        },
        {
          nome: 'vc',
          tipo: 'vetorColuna',
          requerido: false,
        },
        {
          nome: 'vl',
          tipo: 'vetorLinha',
          requerido: false,
        },
      ],
    }
    const resultado = validarEntradas(
      { a: null, f: null, M: null, A: null, b: null, B: null, vc: null, vl: null },
      validacao,
    )
    expect(resultado).toEqual(true)
  })
})
