import { validarNumero } from '../validarNumero'
import { create, all } from 'mathjs'
const math = create(all)

describe('Validar Numero', () => {
  it('Parâmetro a deve ser obrigatório', () => {
    const requisito = {
      nome: 'a',
      tipo: 'number',
      requerido: true,
      condicoes: [
        {
          operador: '<=',
          valor: 10,
        },
      ],
    }
    try {
      validarNumero('testJest', requisito)
    } catch (e) {
      expect(e.message).toEqual(`Função testJest(): o parâmetro 'a' é obrigatório.`)
    }
  })
  it('Parâmetro a deve ser um número', () => {
    const requisito = {
      nome: 'a',
      tipo: 'number',
      requerido: true,
      condicoes: [
        {
          operador: '<=',
          valor: 10,
        },
      ],
    }
    try {
      validarNumero('testJest', requisito, '3')
    } catch (e) {
      expect(e.message).toEqual(`Função testJest(): o parâmetro 'a' deve ser um número.`)
    }
  })
  it('Parâmetro a deve ser menor ou igual a 20', () => {
    const requisito = {
      nome: 'a',
      tipo: 'number',
      requerido: true,
      condicoes: [
        {
          operador: '<=',
          valor: 20,
        },
      ],
    }
    try {
      validarNumero('testJest', requisito, 20.01)
    } catch (e) {
      expect(e.message).toContain('condição')
    }
  })
  it('Parâmetro não é obrigatório e não passa pois não é número', () => {
    const requisito = {
      nome: 'a',
      tipo: 'number',
      requerido: false,
    }
    try {
      validarNumero('testJest', requisito, math.complex('2+i'))
    } catch (e) {
      expect(e.message).toEqual(`Função testJest(): o parâmetro 'a' deve ser um número.`)
    }
  })
  it('Parâmetro não é obrigatório. Validação passa - I', () => {
    const requisito = {
      nome: 'a',
      tipo: 'number',
      requerido: false,
      condicoes: [
        {
          operador: '<=',
          valor: 30,
        },
      ],
    }
    const resultado = validarNumero('testJest', requisito)
    expect(resultado).toEqual(true)
  })
  it('Parâmetro não é obrigatório. Validação passa - II', () => {
    const requisito = {
      nome: 'a',
      tipo: 'number',
      requerido: false,
    }
    const resultado = validarNumero('testJest', requisito, 3)
    expect(resultado).toEqual(true)
  })
  it('Parâmetro não é obrigatório e é enviado. Validação passa', () => {
    const requisito = {
      nome: 'a',
      tipo: 'number',
      requerido: false,
    }
    const resultado = validarNumero('testJest', requisito, 20)
    expect(resultado).toEqual(true)
  })
  it('Obedece a várias condições. Validação passa', () => {
    const requisito = {
      nome: 'a',
      tipo: 'number',
      requerido: false,
      condicoes: [
        {
          operador: '>=',
          valor: 19,
        },
        {
          operador: '<',
          valor: 21,
        },
      ],
    }
    const resultado = validarNumero('testJest', requisito, 20)
    expect(resultado).toEqual(true)
  })
})
