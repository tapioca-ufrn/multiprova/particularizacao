import { validarMatrizesMesmaDim } from '../validarMatrizesMesmaDim'
import { create, all } from 'mathjs'
const math = create(all)

describe('Testa se matrizes tem mesma dimensão', () => {
  it('Sucesso', () => {
    const A = [
      [2, 3],
      [3, 4],
    ]
    const B = [
      [5, 6],
      [7, 8],
    ]
    const requisito = {
      matrizA: 'A',
      matrizB: 'B',
      tipo: 'matrizesMesmaDimensao',
      requerido: true,
    }

    const resultado = validarMatrizesMesmaDim('testJest', requisito, A, B)
    expect(resultado).toEqual(true)
  })

  it('Sucesso 2', () => {
    const A = math.matrix([
      [2, 3],
      [3, 4],
    ])
    const B = math.matrix([
      [5, 6],
      [7, 8],
    ])
    const requisito = {
      matrizA: 'A',
      matrizB: 'B',
      tipo: 'matrizesMesmaDimensao',
      requerido: true,
    }

    const resultado = validarMatrizesMesmaDim('testJest', requisito, A, B)
    expect(resultado).toEqual(true)
  })

  it('Sucesso pois A e b são opcionais e não foram enviadas', () => {
    const requisito = {
      matrizA: 'A',
      matrizB: 'B',
      tipo: 'matrizesMesmaDimensao',
      requerido: false,
    }
    const resultado = validarMatrizesMesmaDim('testJest', requisito)
    expect(resultado).toEqual(true)
  })

  it('Falha pois B não é dada e A e B são obrigatórias', () => {
    const A = [
      [2, 3],
      [3, 4],
    ]
    const requisito = {
      matrizA: 'A',
      matrizB: 'B',
      tipo: 'matrizesMesmaDimensao',
      requerido: true,
    }
    try {
      validarMatrizesMesmaDim('testJest', requisito, A, null)
    } catch (e) {
      expect(e.message).toContain(`Função testJest(): as matrizes 'A' e 'B' são obrigatórias.`)
    }
  })

  it('Falha pois B não é dada', () => {
    const A = [
      [2, 3],
      [3, 4],
    ]
    const requisito = {
      matrizA: 'A',
      matrizB: 'B',
      tipo: 'matrizesMesmaDimensao',
      requerido: false,
    }
    try {
      validarMatrizesMesmaDim('testJest', requisito, A, null)
    } catch (e) {
      expect(e.message).toContain(`Função testJest(): os parâmetros 'A' e 'B' devem ser matrizes.`)
    }
  })

  it('Falha pois A e B são obrigatórias', () => {
    const requisito = {
      matrizA: 'A',
      matrizB: 'B',
      tipo: 'matrizesMesmaDimensao',
      requerido: true,
    }
    try {
      validarMatrizesMesmaDim('testJest', requisito, null, null)
    } catch (e) {
      expect(e.message).toContain(`Função testJest(): as matrizes 'A' e 'B' são obrigatórias.`)
    }
  })

  it('Falha pois matrizes tem dimensões incompatíveis', () => {
    const A = [
      [2, 3],
      [3, 4],
    ]
    const B = [[5], [7]]
    const requisito = {
      matrizA: 'A',
      matrizB: 'B',
      tipo: 'matrizesMesmaDimensao',
      requerido: true,
    }
    try {
      validarMatrizesMesmaDim('testJest', requisito, A, B)
    } catch (e) {
      expect(e.message).toContain('devem ter a mesma dimensão')
    }
  })

  it('Insucesso, pois b é um número', () => {
    const A = [
      [2, 3],
      [3, 4],
    ]
    const b = 5
    const requisito = {
      matrizA: 'A',
      matrizB: 'b',
      tipo: 'matrizesMesmaDimensao',
      requerido: true,
    }
    try {
      validarMatrizesMesmaDim('testJest', requisito, A, b)
    } catch (e) {
      expect(e.message).toContain('devem ser matrizes')
    }
  })
})
