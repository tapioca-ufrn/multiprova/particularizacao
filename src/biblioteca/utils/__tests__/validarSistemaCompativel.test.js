import { validarSistemaCompativel } from '../validarSistemaCompativel'

describe('Testa se matrizes tem mesma dimensão', () => {
  it('Sucesso', () => {
    const A = [
      [2, 3],
      [3, 4],
    ]
    const b = [[5], [7]]
    const requisito = {
      matrizA: 'A',
      matrizB: 'b',
      tipo: 'sistemaCompativel',
      requerido: true,
    }

    const resultado = validarSistemaCompativel('testJest', requisito, A, b)
    expect(resultado).toEqual(true)
  })

  it('Sucesso, pois A e b não são requeridos e são null', () => {
    const requisito = {
      matrizA: 'A',
      matrizB: 'b',
      tipo: 'sistemaCompativel',
      requerido: false,
    }
    const resultado = validarSistemaCompativel('testJest', requisito)
    expect(resultado).toEqual(true)
  })

  it('Falha pois A foi dada e b é null', () => {
    const A = [
      [2, 3],
      [3, 4],
    ]
    const requisito = {
      matrizA: 'A',
      matrizB: 'B',
      tipo: 'sistemaCompativel',
      requerido: false,
    }
    try {
      validarSistemaCompativel('testJest', requisito, A, null)
    } catch (e) {
      expect(e.message).toContain(`Função testJest(): os parâmetros 'A' e 'B' devem ser matrizes.`)
    }
  })

  it('Falha pois A foi dada e b é null e é requerido', () => {
    const A = [
      [2, 3],
      [3, 4],
    ]
    const requisito = {
      matrizA: 'A',
      matrizB: 'B',
      tipo: 'sistemaCompativel',
      requerido: true,
    }
    try {
      validarSistemaCompativel('testJest', requisito, A, null)
    } catch (e) {
      expect(e.message).toContain(`Função testJest(): as matrizes 'A' e 'B' são obrigatórias.`)
    }
  })

  it('Falha pois matrizes tem dimensões incompatíveis', () => {
    const A = [
      [2, 3],
      [3, 4],
    ]
    const B = [
      [5, 6],
      [7, 8],
    ]
    const requisito = {
      matrizA: 'A',
      matrizB: 'B',
      tipo: 'sistemaCompativel',
      requerido: true,
    }
    try {
      validarSistemaCompativel('testJest', requisito, A, B)
    } catch (e) {
      expect(e.message).toContain('devem ter dimensões compatíveis')
    }
  })

  it('Insucesso, pois b é um vetor linha', () => {
    const A = [
      [2, 3],
      [3, 4],
    ]
    const b = [5, 6]
    const requisito = {
      matrizA: 'A',
      matrizB: 'b',
      tipo: 'sistemaCompativel',
      requerido: true,
    }
    try {
      validarSistemaCompativel('testJest', requisito, A, b)
    } catch (e) {
      expect(e.message).toContain('devem ter dimensões compatíveis')
    }
  })
  it('Insucesso, pois b é um número', () => {
    const A = [
      [2, 3],
      [3, 4],
    ]
    const b = 5
    const requisito = {
      matrizA: 'A',
      matrizB: 'b',
      tipo: 'sistemaCompativel',
      requerido: true,
    }
    try {
      validarSistemaCompativel('testJest', requisito, A, b)
    } catch (e) {
      expect(e.message).toContain('devem ser matrizes')
    }
  })

  it('Insucesso, pois A não é quadrada', () => {
    const A = [
      [2, 3],
      [3, 4],
      [5, 6],
    ]
    const b = [[5], [7]]
    const requisito = {
      matrizA: 'A',
      matrizB: 'b',
      tipo: 'sistemaCompativel',
      requerido: true,
    }
    try {
      validarSistemaCompativel('testJest', requisito, A, b)
    } catch (e) {
      expect(e.message).toContain('devem ter dimensões compatíveis')
    }
  })
})
