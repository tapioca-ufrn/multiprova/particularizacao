import { decomposicaoLu } from '../decomposicaoLu'
import { create, all } from 'mathjs'
const math = create(all)

describe('Decomposição LU', () => {
  it('Faz a decomposição corretamente', () => {
    const resultado = decomposicaoLu([
      [-2, 4, 1],
      [-4, 14, 4],
      [2, -10, -2],
    ])
    expect(math.sum(resultado.L)).toEqual(3)
    expect(math.sum(resultado.U)).toEqual(12)
  })
  it('Matriz tem decomposição LU apesar do determinante ser zero', () => {
    const resultado = decomposicaoLu([
      [-2, 4, 1],
      [-4, 14, 4],
      [2, -10, -3],
    ])
    expect(math.sum(resultado.L)).toEqual(3)
    expect(math.sum(resultado.U)).toEqual(11)
  })
  it('Matriz A não tem decomposição LU', () => {
    try {
      decomposicaoLu([
        [-2, 4, 1],
        [2, -4, 4],
        [2, -10, -2],
      ])
    } catch (e) {
      expect(e.message).toEqual('Matriz não possui decomposição LU')
    }
  })
})
