import { validarVetorLinha } from '../validarVetorLinha'
import { create, all } from 'mathjs'
const math = create(all)

describe('Testa se o argumento é um vetor linha', () => {
  it('Sucesso', () => {
    const v = [[2, 3, 4]]
    const requisito = {
      nome: 'v',
      tipo: 'vetorLinha',
      requerido: true,
    }

    const resultado = validarVetorLinha('testJest', requisito, v)
    expect(resultado).toEqual(true)
  })

  it('Sucesso 2', () => {
    const v = math.matrix([[2, 3, 4]])
    const requisito = {
      nome: 'v',
      tipo: 'vetorLinha',
      requerido: true,
    }

    const resultado = validarVetorLinha('testJest', requisito, v)
    expect(resultado).toEqual(true)
  })

  it('Sucesso, pois v é null', () => {
    const requisito = {
      nome: 'v',
      tipo: 'vetorLinha',
      requerido: false,
    }
    const resultado = validarVetorLinha('testJest', requisito)
    expect(resultado).toEqual(true)
  })

  it('Falha pois v é null e é requerida', () => {
    const requisito = {
      nome: 'v',
      tipo: 'vetorLinha',
      requerido: true,
    }
    try {
      validarVetorLinha('testJest', requisito, null)
    } catch (e) {
      expect(e.message).toContain(`Função testJest(): o vetor linha 'v' é obrigatório.`)
    }
  })

  it('Falha pois v não é um vetor linha', () => {
    const v = [
      [2, 3],
      [3, 4],
    ]
    const requisito = {
      nome: 'v',
      tipo: 'vetorLinha',
      requerido: true,
    }
    try {
      validarVetorLinha('testJest', requisito, v)
    } catch (e) {
      expect(e.message).toContain('deve estar no formato de um vetor linha')
    }
  })

  it('Falha pois v não é um vetor linha - tem 3 dimensões', () => {
    const v = [[[3]]]
    const requisito = {
      nome: 'v',
      tipo: 'vetorLinha',
      requerido: true,
    }
    try {
      validarVetorLinha('testJest', requisito, v)
    } catch (e) {
      expect(e.message).toContain('deve estar no formato de um vetor linha')
    }
  })

  it('Insucesso, pois v é um vetor colunha', () => {
    const v = [[3], [4]]
    const requisito = {
      nome: 'v',
      tipo: 'vetorLinha',
      requerido: true,
    }
    try {
      validarVetorLinha('testJest', requisito, v)
    } catch (e) {
      expect(e.message).toContain('deve estar no formato de um vetor linha')
    }
  })
  it('Insucesso, pois v é um número', () => {
    const v = 5
    const requisito = {
      nome: 'v',
      tipo: 'vetorLinha',
      requerido: true,
    }
    try {
      validarVetorLinha('testJest', requisito, v)
    } catch (e) {
      expect(e.message).toContain('deve ser um matriz (vetor linha).')
    }
  })
})
