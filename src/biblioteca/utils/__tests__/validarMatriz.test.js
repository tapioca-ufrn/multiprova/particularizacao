import { validarMatriz } from '../validarMatriz'

describe('Validar Numero', () => {
  it('Parâmetro A deve ser obrigatório', () => {
    const requisito = {
      nome: 'A',
      tipo: 'Matrix',
      requerido: true,
    }
    try {
      validarMatriz('testJest', requisito)
    } catch (e) {
      expect(e.message).toEqual(`Função testJest(): o parâmetro 'A' é uma matriz obrigatória.`)
    }
  })
  it('Parâmetro a deve ser uma matriz', () => {
    const requisito = {
      nome: 'A',
      tipo: 'Matrix',
      requerido: true,
    }
    try {
      validarMatriz('testJest', requisito, 2)
    } catch (e) {
      expect(e.message).toEqual(`Função testJest(): o parâmetro 'A' deve ser uma matriz. Atualmente é do tipo: number.`)
    }
  })
  it('A é uma matriz. Validação passa', () => {
    const requisito = {
      nome: 'A',
      tipo: 'Matriz',
      requerido: true,
    }
    const resultado = validarMatriz('testJest', requisito, [
      [1, 2],
      [3, 4],
    ])
    expect(resultado).toEqual(true)
  })
})
