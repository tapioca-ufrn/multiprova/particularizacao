import { validarVetorColuna } from '../validarVetorColuna'
import { create, all } from 'mathjs'
const math = create(all)

describe('Testa se o argumento é um vetor coluna', () => {
  it('Sucesso', () => {
    const v = [[5], [7]]
    const requisito = {
      nome: 'v',
      tipo: 'vetorColuna',
      requerido: true,
    }

    const resultado = validarVetorColuna('testJest', requisito, v)
    expect(resultado).toEqual(true)
  })

  it('Sucesso 2 - com matriz', () => {
    const v = math.matrix([[5], [7]])
    const requisito = {
      nome: 'v',
      tipo: 'vetorColuna',
      requerido: true,
    }

    const resultado = validarVetorColuna('testJest', requisito, v)
    expect(resultado).toEqual(true)
  })

  it('Sucesso, pois v é null', () => {
    const requisito = {
      nome: 'v',
      tipo: 'vetorColuna',
      requerido: false,
    }
    const resultado = validarVetorColuna('testJest', requisito)
    expect(resultado).toEqual(true)
  })

  it('Falha pois v é null e é requerida', () => {
    const requisito = {
      nome: 'v',
      tipo: 'vetorColuna',
      requerido: true,
    }
    try {
      validarVetorColuna('testJest', requisito, null)
    } catch (e) {
      expect(e.message).toContain(`Função testJest(): o vetor coluna 'v' é obrigatório.`)
    }
  })

  it('Falha pois v não é um vetor coluna', () => {
    const v = [
      [2, 3],
      [3, 4],
    ]
    const requisito = {
      nome: 'v',
      tipo: 'vetorColuna',
      requerido: true,
    }
    try {
      validarVetorColuna('testJest', requisito, v)
    } catch (e) {
      expect(e.message).toContain('deve estar no formato de um vetor coluna')
    }
  })

  it('Falha pois v não é um vetor coluna - tem 3 dimensões', () => {
    const v = [[[3]]]
    const requisito = {
      nome: 'v',
      tipo: 'vetorColuna',
      requerido: true,
    }
    try {
      validarVetorColuna('testJest', requisito, v)
    } catch (e) {
      expect(e.message).toContain('deve estar no formato de um vetor coluna')
    }
  })

  it('Insucesso, pois v é um vetor linha', () => {
    const v = [5, 6]
    const requisito = {
      nome: 'v',
      tipo: 'vetorColuna',
      requerido: true,
    }
    try {
      validarVetorColuna('testJest', requisito, v)
    } catch (e) {
      expect(e.message).toContain('deve estar no formato de um vetor coluna')
    }
  })
  it('Insucesso, pois v é um número', () => {
    const v = 5
    const requisito = {
      nome: 'v',
      tipo: 'vetorColuna',
      requerido: true,
    }
    try {
      validarVetorColuna('testJest', requisito, v)
    } catch (e) {
      expect(e.message).toContain('deve ser um matriz (vetor coluna).')
    }
  })
})
