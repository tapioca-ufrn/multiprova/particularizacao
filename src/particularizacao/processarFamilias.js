import { sortearValoresFamilia } from './sortearValoresFamilia'
import { formatarNumero } from './formatarNumero'
import { definirSeparacaoMinima } from './definirSeparacaoMinima'
import { codigosErros } from './utils/codigosErros'
import { tiposMathjs } from './utils/tiposMathjs'
import { ParticularizaError } from './utils/ParticularizaError'
import { tiposValoresPermitidos } from './utils/tiposValoresPermitidos'
import { constantesNaoPermitidas } from './utils/constantesNaoPermitidas'

import { create, all } from 'mathjs'
import { funcoes } from '../biblioteca'

const math = create(all)
math.import(funcoes)

const desrespeitaSeparacaoMinima = (valoresCalculados, separacaoMinima, casasDecimais) => {
  let desrespeita = false
  const valoresCalculadosOrdenados = math.sort(valoresCalculados)
  for (let i = 0; i < valoresCalculadosOrdenados.length - 1; i++) {
    const subtracao = math.round(valoresCalculadosOrdenados[i + 1] - valoresCalculadosOrdenados[i], casasDecimais)

    if (subtracao < separacaoMinima) {
      desrespeita = true
      break
    }
  }
  return desrespeita
}

const testarSeparacaoMinima = (
  id,
  separacaoMinimaAbsoluta,
  separacaoMinimaRelativa,
  casasDecimais,
  valorMaximoSorteio,
  valorMinimoSorteio,
  valoresCalculadosFuncoes,
) => {
  const separacaoMinima = definirSeparacaoMinima(
    separacaoMinimaAbsoluta,
    separacaoMinimaRelativa,
    casasDecimais,
    valorMaximoSorteio,
    valorMinimoSorteio,
    valoresCalculadosFuncoes,
  )

  if (desrespeitaSeparacaoMinima(valoresCalculadosFuncoes, separacaoMinima, casasDecimais)) {
    throw new ParticularizaError({
      id: id,
      code: codigosErros.funcaoSeparacaoMinimaNaoRespeitada,
      info: String(separacaoMinima),
    })
  }
}

const processarSorteiosFamilia = (familia, valoresASeremSorteados, valoresFuncoes, escopo) => {
  const { casasDecimais, separadorDecimal, separadorMilhar } = familia

  const valoresSorteados = sortearValoresFamilia(familia, valoresFuncoes)

  const resultadoSorteio = []
  let i = 0
  for (const valorSorteado of valoresASeremSorteados) {
    const valorFormatado = formatarNumero(valoresSorteados[i], casasDecimais, separadorDecimal, separadorMilhar)
    resultadoSorteio.push({
      id: valorSorteado.id,
      nome: valorSorteado.nome,
      tipo: 'familiaValorSorteado',
      ...valorFormatado,
    })
    escopo = { ...escopo, [valorSorteado.nome]: valorFormatado.valor }
    i++
  }
  return { resultado: resultadoSorteio, escopo: escopo }
}

const validarValorCalculadoFuncao = (valor, id) => {
  if (constantesNaoPermitidas.includes(String(valor))) {
    throw new ParticularizaError({
      id: id,
      code: codigosErros.funcaoResultadoInvalido,
      info: String(valor),
    })
  }
  if (!tiposValoresPermitidos.includes(math.typeOf(valor))) {
    throw new ParticularizaError({
      id: id,
      code: codigosErros.funcaoResultadoTipoInvalido,
      info: math.typeOf(valor),
    })
  }
}

const executarCalculoFuncao = (id, funcao, escopo) => {
  let valor
  try {
    valor = math.evaluate(funcao, escopo)
  } catch (error) {
    throw new ParticularizaError({
      id: id,
      code: codigosErros.funcaoConteudoInvalido,
      info: error.message,
    })
  }

  validarValorCalculadoFuncao(valor, id)
  return valor
}

const calcularFuncao = (escopo, funcao, casasDecimais, separadorDecimal, separadorMilhar) => {
  let valor = executarCalculoFuncao(funcao.id, funcao.formula, escopo)

  valor = math.round(valor, casasDecimais)
  escopo = { ...escopo, [funcao.nome]: valor }

  const resultado = {
    id: funcao.id,
    nome: funcao.nome,
    tipo: 'familiaValorCalculado',
    ...formatarNumero(valor, casasDecimais, separadorDecimal, separadorMilhar),
  }
  return { escopo: escopo, resultado: resultado }
}

const obterTipoValoresCalculados = (id, valores) => {
  if (valores.length === 1) return math.typeOf(valores[0].valor)

  let tipo = false
  for (let i = 0; i < valores.length - 1; i++) {
    if (math.typeOf(valores[i].valor) === math.typeOf(valores[i + 1].valor)) {
      tipo = math.typeOf(valores[i].valor)
    } else {
      throw new ParticularizaError({
        id: id,
        code: codigosErros.funcaoResultadosTiposDiferentes,
        info: math.typeOf(valores[i].valor) + ' - ' + math.typeOf(valores[i + 1].valor),
      })
    }
  }
  return tipo
}

const processarFuncoesDaFamilia = (escopo, funcoes, familia) => {
  const {
    id,
    casasDecimais,
    separadorDecimal,
    separadorMilhar,
    separacaoMinimaAbsoluta,
    separacaoMinimaRelativa,
    valorMaximoSorteio,
    valorMinimoSorteio,
  } = familia

  let valoresCalculadosFuncoes = []
  let vetorFamiliaCalculada = []
  for (const funcao of funcoes) {
    if (String(funcao.formula).trim() === '') continue

    const objetoCalculoDaFuncao = calcularFuncao(escopo, funcao, casasDecimais, separadorDecimal, separadorMilhar)
    valoresCalculadosFuncoes.push(objetoCalculoDaFuncao.resultado.valor)

    escopo = objetoCalculoDaFuncao.escopo
    vetorFamiliaCalculada.push(objetoCalculoDaFuncao.resultado)
  }

  const tipoValoresCalculados = obterTipoValoresCalculados(id, vetorFamiliaCalculada)
  if (tipoValoresCalculados === tiposMathjs.number || tipoValoresCalculados === tiposMathjs.bigNumber) {
    testarSeparacaoMinima(
      id,
      separacaoMinimaAbsoluta,
      separacaoMinimaRelativa,
      casasDecimais,
      valorMaximoSorteio,
      valorMinimoSorteio,
      valoresCalculadosFuncoes,
    )
  }

  if (tipoValoresCalculados === tiposMathjs.number || tipoValoresCalculados === tiposMathjs.bigNumber) {
    testarSeparacaoMinima(
      id,
      separacaoMinimaAbsoluta,
      separacaoMinimaRelativa,
      casasDecimais,
      valorMaximoSorteio,
      valorMinimoSorteio,
      valoresCalculadosFuncoes,
    )
  }

  return {
    escopo: escopo,
    resultado: vetorFamiliaCalculada,
    valoresFuncoes: valoresCalculadosFuncoes,
    tipoValoresCalculados: tipoValoresCalculados,
  }
}

const processarFamilia = (escopo, familia) => {
  familia.quantidadeValoresSorteados = familia.valoresSorteados.length
  if (familia.separacaoMinimaAbsoluta == null) familia.separacaoMinimaAbsoluta = 0
  if (familia.separacaoMinimaRelativa == null) familia.separacaoMinimaRelativa = 0

  const resultadoProcessamentoFuncoes = processarFuncoesDaFamilia(escopo, familia.valoresCalculados, familia)
  let resultadosFamilia = resultadoProcessamentoFuncoes.resultado
  escopo = resultadoProcessamentoFuncoes.escopo

  const tipoValores = resultadoProcessamentoFuncoes.tipoValoresCalculados
  if (
    tipoValores !== tiposMathjs.number &&
    tipoValores !== tiposMathjs.bigNumber &&
    tipoValores !== false &&
    familia.valoresSorteados.length
  ) {
    throw new ParticularizaError({
      id: familia.id,
      code: codigosErros.sorteiaSomenteTiposNumericos,
      info: tipoValores,
    })
  }

  if (tipoValores === tiposMathjs.number || tipoValores === tiposMathjs.bigNumber || resultadosFamilia.length == 0) {
    const resultadoSorteioFamilia = processarSorteiosFamilia(
      familia,
      familia.valoresSorteados,
      resultadoProcessamentoFuncoes.valoresFuncoes,
      escopo,
    )
    resultadosFamilia = resultadosFamilia.concat(resultadoSorteioFamilia.resultado)
    escopo = resultadoSorteioFamilia.escopo
  }

  return { escopo: escopo, resultado: resultadosFamilia }
}

export const processarFamilias = (escopo, familias) => {
  let resultadosTodasFamilias = []

  for (const familia of familias) {
    let resultadoProcessamentoFamilia
    try {
      resultadoProcessamentoFamilia = processarFamilia(escopo, familia)
    } catch (erro) {
      throw new ParticularizaError({
        id: erro.id || familia.id,
        code: erro.code || codigosErros.erroDesconhecido,
        info: erro.info || erro.message,
      })
    }
    

    escopo = resultadoProcessamentoFamilia.escopo
    resultadosTodasFamilias = resultadosTodasFamilias.concat(resultadoProcessamentoFamilia.resultado)
  }

  return { escopo: escopo, resultado: resultadosTodasFamilias }
}
