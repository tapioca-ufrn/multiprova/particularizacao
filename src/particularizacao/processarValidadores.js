import { ParticularizaError } from './utils/ParticularizaError'
import { codigosErros } from './utils/codigosErros'
import { create, all } from 'mathjs'
const math = create(all)

const calcularValidador = (validador, escopo) => {
  const expressao = validador.formula + validador.simbolo
  return math.evaluate(expressao, escopo)
}

export const processarValidadores = (validadores, escopo) => {
  const errosValidacao = []
  let semErrosValidacao = true
  for (const validador of validadores) {
    if (String(validador.formula).trim() === '') continue
    try {
      const resultado = calcularValidador(validador, escopo)
      if (resultado === false) {
        semErrosValidacao = false
        errosValidacao.push({
          id: validador.id,
          code: codigosErros.validadorNaoFoiSatisfeito,
          info: String(validador.formula) + String(validador.simbolo),
        })
      }
    } catch (e) {
      semErrosValidacao = false
      errosValidacao.push({
        id: validador.id,
        code: codigosErros.validadorConteudoInvalido,
        info: e.message,
      })
    }
  }
  if (semErrosValidacao === false) {
    throw new ParticularizaError({ errosArray: errosValidacao })
  }
  return semErrosValidacao
}

export const processarValidadoresAposVariaveis = (validadores, escopo) => {
  const errosValidacao = []
  let semErrosValidacao = true
  for (const validador of validadores) {
    if (String(validador.formula).trim() === '') continue
    try {
      const resultado = calcularValidador(validador, escopo)
      if (resultado === false) {
        semErrosValidacao = false
        errosValidacao.push({
          id: validador.id,
          code: codigosErros.validadorConteudoParcialmenteInvalido,
          info: String(validador.formula) + String(validador.simbolo),
        })
      }
    } catch (e) {
      semErrosValidacao = true
    }
  }
  if (semErrosValidacao === false) {
    throw new ParticularizaError({ errosArray: errosValidacao })
  }
  return semErrosValidacao
}

export const isErroValidacaoDefinitivo = (erros) => {
  let definitivo = false
  for (let erro of erros) {
    if (erro.code === codigosErros.validadorConteudoInvalido) {
      definitivo = true
      break
    }
  }
  return definitivo
}
