import { create, all } from 'mathjs'

const math = create(all)

export const definirSeparacaoMinima = (
  separacaoMinimaAbsoluta,
  separacaoMinimaRelativa,
  casasDecimais,
  valorMaximoSorteio,
  valorMinimoSorteio,
  valoresCalculadosArredondados,
) => {
  const separacaoDecimal = math.pow(10, -casasDecimais)
  const maximoValorCalculado = valoresCalculadosArredondados.length
    ? math.max(math.abs(valoresCalculadosArredondados))
    : math.max(math.abs(valorMaximoSorteio), math.abs(valorMinimoSorteio))

  const separacaoMinimaDecorrenteDeSeparacaoMinimaRelativa = math.round(
    (separacaoMinimaRelativa * maximoValorCalculado) / 100,
    casasDecimais,
  )

  const separacaoMinima = math.max(
    separacaoMinimaAbsoluta,
    separacaoMinimaDecorrenteDeSeparacaoMinimaRelativa,
    separacaoDecimal,
  )
  return separacaoMinima
}
