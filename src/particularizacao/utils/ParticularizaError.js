export class ParticularizaError extends Error {
  constructor(erros) {
    super('Erros de particularização de questões')
    this.id = erros.id || undefined
    this.code = erros.code || undefined
    this.info = erros.info || undefined
    this.errosArray = erros.errosArray || []
  }
}
