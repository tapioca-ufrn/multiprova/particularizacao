export const tiposMathjs = {
  number: 'number',
  bigNumber: 'BigNumber',
  matrix: 'Matrix',
  array: 'Array',
  complex: 'Complex',
  unit: 'Unit',
  infinity: 'Infinity',
  nan: 'NaN',
}
