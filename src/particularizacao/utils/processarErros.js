import { codigosErros } from './codigosErros'

const isInArray = (valor, array) => {
  return array.indexOf(valor) > -1
}

export const consolidarErros = (todosErros) => {
  const codigosIdsErrosUnivocos = []
  const errosParaEnviar = []
  for (const erro of todosErros) {
    if (isInArray(erro.code + erro.id, codigosIdsErrosUnivocos)) continue
    errosParaEnviar.push({
      id: erro.id,
      code: erro.code,
      info: erro.info,
    })
    codigosIdsErrosUnivocos.push(erro.code + erro.id)
  }
  return errosParaEnviar
}

export const isErroDefinitivo = (codigoErro) => {
  const errosDefinitivos = [codigosErros.funcaoConteudoInvalido, codigosErros.validadorConteudoInvalido]

  for (let codigo of errosDefinitivos) {
    if (codigoErro === codigo) return true
  }
  return false
}
