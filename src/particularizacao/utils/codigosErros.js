export const codigosErros = {
  funcaoConteudoInvalido: 'conteudo_invalido_da_funcao',
  funcaoResultadoInvalido: 'funcao_resultado_invalido',
  funcaoResultadoTipoInvalido: 'funcao_resultado_tipo_invalido',
  funcaoResultadosTiposDiferentes: 'funcao_resultados_tipos_diferentes',
  funcaoSorteioImpossivel: 'impossivel_sortear_valores_para_a_familia',
  sorteiaSomenteTiposNumericos: 'sorteia_somente_tipos_numericos',
  funcaoSeparacaoMinimaNaoRespeitada: 'separacao_minima_nao_respeitada_para_as_funcoes_da_familia',
  validadorConteudoParcialmenteInvalido: 'conteudo_parcialmente_invalido_do_validador',
  validadorConteudoInvalido: 'conteudo_invalido_do_validador',
  validadorNaoFoiSatisfeito: 'validador_nao_foi_satisfeito',
  erroDesconhecido: 'erro_desconhecido',
}
