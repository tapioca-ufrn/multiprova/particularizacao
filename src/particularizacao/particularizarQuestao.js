import { processarVariaveis } from './processarVariaveis'
import { processarFamilias } from './processarFamilias'
import {
  processarValidadores,
  processarValidadoresAposVariaveis,
  isErroValidacaoDefinitivo,
} from './processarValidadores'
import { ParticularizaError } from './utils/ParticularizaError'
import { isErroDefinitivo, consolidarErros } from './utils/processarErros'
import { codigosErros } from './utils/codigosErros'

const iniciarTentativasParticularizarQuestao = (acessorios, nMaxTentativas) => {
  let sucessoParticularizacao = false
  const numeroMaximoTentativas = nMaxTentativas || 500
  let numeroTentativas = 1

  let resultado
  let escopo
  let todosOsErros = []
  while (sucessoParticularizacao === false && numeroTentativas <= numeroMaximoTentativas) {
    const resultadoProcessamentoVariaveis = processarVariaveis(acessorios.variaveis)
    resultado = resultadoProcessamentoVariaveis.resultado
    escopo = resultadoProcessamentoVariaveis.escopo

    try {
      processarValidadoresAposVariaveis(acessorios.validadores, escopo)
    } catch (erro) {
      todosOsErros = todosOsErros.concat(erro.errosArray)
      numeroTentativas += 1
      continue
    }

    let resultadoProcessamentoFamilias
    try {
      resultadoProcessamentoFamilias = processarFamilias(escopo, acessorios.familias)
      escopo = { ...resultadoProcessamentoFamilias.escopo }
      resultado = resultado.concat(resultadoProcessamentoFamilias.resultado)
    } catch (erro) {
      numeroTentativas += 1
      todosOsErros.push(erro)
      if (isErroDefinitivo(erro.code)) break
      continue
    }

    try {
      const sucesso = processarValidadores(acessorios.validadores, escopo)
      if (sucesso === true) {
        sucessoParticularizacao = true
      }
    } catch (erro) {
      todosOsErros = todosOsErros.concat(erro.errosArray)
      numeroTentativas += 1
      if (isErroValidacaoDefinitivo(erro.errosArray) === true) break
      continue
    }
  }

  if (sucessoParticularizacao === false) {
    throw new ParticularizaError({
      errosArray: consolidarErros(todosOsErros),
    })
  }

  return resultado
}

export const particularizarQuestao = (acessorios, nMaxTentativas) => {
  try {
    return iniciarTentativasParticularizarQuestao(acessorios, nMaxTentativas)
  } catch (erro) {
    if (erro.errosArray) {
      throw new ParticularizaError({
        errosArray: erro.errosArray,
      })
    } else {
      throw new ParticularizaError({
        errosArray: [
          {
            id: null,
            code: codigosErros.erroDesconhecido,
            info: erro,
          },
        ],
      })
    }
  }
}
