import { processarFamilias } from '../processarFamilias'
import { codigosErros } from '../utils/codigosErros'
import { create, all } from 'mathjs'
const math = create(all)

const familias = [
  {
    id: 'd0d22c31-0ba7-4247-a636-f878df35f756',
    nome: 'F',
    casasDecimais: 2,
    valoresCalculados: [
      {
        id: 'ef6afb12-c1c0-43b9-9a96-924bdcab5520',
        nome: 'f1',
        formula: '2',
      },
      {
        id: 'ff6afb12-c1c0-43b9-9a96-924bdcab5520',
        nome: 'f2',
        formula: '4',
      },
    ],
    valoresSorteados: [],
    separacaoMinimaAbsoluta: 3,
    separacaoMinimaRelativa: 1,
    valorMinimoSorteio: 1,
    valorMaximoSorteio: 8.12,
    separadorDecimal: ',',
    separadorMilhar: '.',
  },
]

const comTodosTipos = [
  {
    id: '3467bb5a-49f5-45b8-aaf5-ca4d94469f4c',
    nome: 'f',
    casasDecimais: 3,
    separadorDecimal: ',',
    separadorMilhar: '',
    separacaoMinimaAbsoluta: null,
    separacaoMinimaRelativa: null,
    valorMinimoSorteio: -5,
    valorMaximoSorteio: 10,
    valoresSorteados: [
      {
        id: '5672e9g0-96fc-40a5-afb5-96d934c235a7',
        nome: 'F1',
      },
      {
        id: '355dd554-96fc-40a5-afb5-96d934c235a7',
        nome: 'F2',
      },
    ],
    valoresCalculados: [
      {
        id: '2f56d5be-4ed9-4b0d-8bfe-2b3711c59c77',
        nome: 'f_1',
        formula: '[[2,2],[3,4]]',
      },
      {
        id: 'f74e547a-625b-4442-a4c8-78bea91d0508',
        nome: 'f_2',
        formula: '2',
      },
      {
        id: '9990dda8-0bfc-4155-b511-ef02397d6901',
        nome: 'f_3',
        formula: '2+i ',
      },
    ],
  },
]
const comTodosTiposIguaisSemSorteio = [
  {
    id: '3467bb5a-49f5-45b8-aaf5-ca4d94469f4c',
    nome: 'f',
    casasDecimais: 0,
    separadorDecimal: ',',
    separadorMilhar: '',
    separacaoMinimaAbsoluta: null,
    separacaoMinimaRelativa: null,
    valorMinimoSorteio: -5,
    valorMaximoSorteio: 10,
    valoresSorteados: [],
    valoresCalculados: [
      {
        id: '2f56d5be-4ed9-4b0d-8bfe-2b3711c59c77',
        nome: 'f_1',
        formula: '[[1,2],[3,4],[0,4]]',
      },
      {
        id: 'f74e547a-625b-4442-a4c8-78bea91d0508',
        nome: 'f_2',
        formula: '[[2,2],[3,4]]',
      },
      {
        id: '9990dda8-0bfc-4155-b511-ef02397d6901',
        nome: 'f_3',
        formula: '[[2,2],[3,4]]',
      },
    ],
  },
]

const comTodosTiposIguaisComSorteio = [
  {
    id: '3467bb5a-49f5-45b8-aaf5-ca4d94469f4c',
    nome: 'f',
    casasDecimais: 0,
    separadorDecimal: ',',
    separadorMilhar: '',
    separacaoMinimaAbsoluta: null,
    separacaoMinimaRelativa: null,
    valorMinimoSorteio: -5,
    valorMaximoSorteio: 10,
    valoresSorteados: [
      {
        id: '5672e9g0-96fc-40a5-afb5-96d934c235a7',
        nome: 'F1',
      },
    ],
    valoresCalculados: [
      {
        id: '2f56d5be-4ed9-4b0d-8bfe-2b3711c59c77',
        nome: 'f_1',
        formula: '[[1,2],[3,4],[0,4]]',
      },
      {
        id: 'f74e547a-625b-4442-a4c8-78bea91d0508',
        nome: 'f_2',
        formula: '[[2,2],[3,4]]',
      },
      {
        id: '9990dda8-0bfc-4155-b511-ef02397d6901',
        nome: 'f_3',
        formula: '[[2,2],[3,4]]',
      },
    ],
  },
]

const comVariosTiposValidos = [
  {
    id: '3467bb5a-49f5-45b8-aaf5-ca4d94469f4c',
    nome: 'f',
    casasDecimais: 0,
    separadorDecimal: ',',
    separadorMilhar: '',
    separacaoMinimaAbsoluta: null,
    separacaoMinimaRelativa: null,
    valorMinimoSorteio: -5,
    valorMaximoSorteio: 10,
    valoresSorteados: [],
    valoresCalculados: [
      {
        id: '2f56d5be-4ed9-4b0d-8bfe-2b3711c59c77',
        nome: 'f_1',
        formula: '[[1,2],[3,4],[0,4]]',
      },
      {
        id: 'f74e547a-625b-4442-a4c8-78bea91d0508',
        nome: 'f_2',
        formula: '[[2,2],[3,4]]',
      },
      {
        id: '9990dda8-0bfc-4155-b511-ef02397d6901',
        nome: 'f_3',
        formula: 'cos(2)',
      },
    ],
  },
]

describe('ProcessarFamilias', () => {
  it('Valores calculados não respeitam separação mínima', () => {
    try {
      processarFamilias({}, familias)
    } catch (e) {
      expect(e.info).toEqual('3')
    }
  })
  it('Familia com um tipo de valor calculado inválido', () => {
    try {
      processarFamilias({}, comTodosTipos)
    } catch (e) {
      expect(e.code).toEqual(codigosErros.funcaoResultadoTipoInvalido)
    }
  })
  it('Familia com cálculo de matrizes sem valores sorteados', () => {
    const resultadoProcessamento = processarFamilias({}, comTodosTiposIguaisSemSorteio)
    expect(math.sum(resultadoProcessamento.resultado[0].valor)).toEqual(14)
  })
  it('Familia com cálculo de matrizes COM valores sorteados', () => {
    try {
      processarFamilias({}, comTodosTiposIguaisComSorteio)
    } catch (e) {
      expect(e.code).toEqual(codigosErros.sorteiaSomenteTiposNumericos)
    }
  })
  it('Funções da família retornam valores de mais de um tipo válido', () => {
    try {
      processarFamilias({}, comVariosTiposValidos)
    } catch (e) {
      expect(e.code).toEqual(codigosErros.funcaoResultadosTiposDiferentes)
    }
  })
})
