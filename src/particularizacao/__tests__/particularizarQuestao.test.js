import { particularizarQuestao } from '../particularizarQuestao'
import { codigosErros } from '../utils/codigosErros'
import { create, all } from 'mathjs'
const math = create(all)

const acessorios = {
  variaveis: [
    {
      id: '46943ed4-9ff1-4774-bb69-a6e353f4290a',
      nome: 'x',
      casasDecimais: 2,
      intervalos: [
        {
          minimo: 0,
          maximo: 3.14159,
        },
      ],
      avulsos: [],
      separadorDecimal: ',',
      separadorMilhar: '.',
    },
    {
      id: '56943ed4-9ff1-4774-bb69-a6e353f4290a',
      nome: 'h',
      casasDecimais: 1,
      intervalos: [
        {
          minimo: 19.9444,
          maximo: 20.1444,
        },
      ],
      avulsos: [100.588, 200.333, 300.58554],
      separadorDecimal: ',',
      separadorMilhar: '.',
    },
  ],
  familias: [
    {
      id: 'd0d22c31-0ba7-4247-a636-f878df35f756',
      nome: 'F',
      casasDecimais: 2,
      valoresCalculados: [
        {
          id: 'ef6afb12-c1c0-43b9-9a96-924bdcab5520',
          nome: 'f1',
          formula: '1*h',
        },
        {
          id: 'ff6afb12-c1c0-43b9-9a96-924bdcab5520',
          nome: 'f2',
          //formula: 'bisseccao(f(h)=h^2-1 -2 , 2, 4, 3, 4)',
          formula: 'abs(cos(x)) + 1.4',
        },
        {
          id: 'ff6afb12-c1c0-43b9-9a96-924bdcab5520',
          nome: 'f3',
          formula: 'f1+14',
        },
      ],
      valoresSorteados: [],
      separacaoMinimaAbsoluta: 1,
      separacaoMinimaRelativa: 1,
      valorMinimoSorteio: 1,
      valorMaximoSorteio: 8.12,
      separadorDecimal: ',',
      separadorMilhar: '.',
    },
    {
      id: 'e0d22c31-0ba7-4247-a636-f878df35f756',
      nome: 'G',
      casasDecimais: 3,
      valoresCalculados: [
        {
          id: 'gg6afb12-c1c0-43b9-9a96-924bdcab5520',
          nome: 'g1',
          formula: 'f3 + 100',
        },
        {
          id: 'hh6afb12-c1c0-43b9-9a96-924bdcab5520',
          nome: 'g2',
          formula: 'g1 + 100',
        },
      ],
      valoresSorteados: [
        {
          id: '5672e9g0-96fc-40a5-afb5-96d934c235a7',
          nome: 'G1',
        },
        {
          id: '355dd554-96fc-40a5-afb5-96d934c235a7',
          nome: 'G2',
        },
      ],
      separacaoMinimaAbsoluta: 1,
      separacaoMinimaRelativa: 1,
      valorMinimoSorteio: 0,
      valorMaximoSorteio: 1000,
      separadorDecimal: ',',
      separadorMilhar: '.',
    },
  ],
  validadores: [
    {
      id: '9b937fed-60d2-48a0-a9a7-0ef1bc8cddd8',
      formula: 'x + 1',
      simbolo: '>=0',
    },
  ],
  tipo: 'naoCalculado',
}

const acessorios2 = {
  variaveis: [
    {
      id: '6a9cc7e7-9fff-447b-bba2-36cfb9e798fc',
      nome: 'x',
      tipo: 'padrao',
      casasDecimais: 0,
      intervalos: [
        {
          minimo: 2,
          maximo: 10,
        },
      ],
      avulsos: [],
      separadorDecimal: ',',
      separadorMilhar: '.',
    },
  ],
  familias: [
    {
      id: '12245d93-cda1-4e4a-b054-881724ce8ea2',
      nome: 'f',
      casasDecimais: 0,
      separadorDecimal: ',',
      separadorMilhar: '.',
      separacaoMinimaAbsoluta: null,
      separacaoMinimaRelativa: null,
      valorMinimoSorteio: null,
      valorMaximoSorteio: null,
      valoresSorteados: [],
      valoresCalculados: [
        {
          id: 'gg6afb12-c1c0-43b9-9a96-924bdcab5520',
          nome: 'f1',
          formula: 'x + 1',
        },
      ],
    },
  ],
  validadores: [
    {
      id: 'ef6afb12-c1c0-43b9-9a96-validador1',
      simbolo: '>=0',
      formula: 'x',
    },
  ],
  status: 'naoCalculado',
}

const acessorios3 = {
  variaveis: [
    {
      id: '6a9cc7e7-9fff-447b-bba2-36cfb9e798fc',
      nome: 'x',
      tipo: 'padrao',
      casasDecimais: 0,
      intervalos: [
        {
          minimo: 2,
          maximo: 10,
        },
      ],
      avulsos: [],
      separadorDecimal: ',',
      separadorMilhar: '.',
    },
  ],
  familias: [],
  validadores: [
    {
      id: 'ef6afb12-c1c0-43b9-9a96-validador2',
      simbolo: '>=0',
      formula: 'x+h+s',
    },
  ],
  status: 'naoCalculado',
}
const acessorios4 = {
  variaveis: [],
  familias: [
    {
      id: 'd0d22c31-0ba7-4247-a636-f878df35f756',
      nome: 'F',
      casasDecimais: 2,
      valoresCalculados: [
        {
          id: 'ef6afb12-c1c0-43b9-9a96-valorNaoDefinido',
          nome: 'f1',
          formula: 'valorNaoDefinido',
        },
      ],
      valoresSorteados: [],
      separacaoMinimaAbsoluta: 1,
      separacaoMinimaRelativa: 1,
      valorMinimoSorteio: 1,
      valorMaximoSorteio: 8.12,
      separadorDecimal: ',',
      separadorMilhar: '.',
    },
  ],
  validadores: [],
  status: 'naoCalculado',
}

const acessorios5 = {
  variaveis: [],
  familias: [
    {
      id: 'd0d22c31-0ba7-4247-a636-f878df35f756',
      nome: 'F',
      casasDecimais: 2,
      valoresCalculados: [
        {
          id: 'ef6afb12-c1c0-43b9-9a96-infinito',
          nome: 'f1',
          formula: '1/0',
        },
      ],
      valoresSorteados: [],
      separacaoMinimaAbsoluta: 1,
      separacaoMinimaRelativa: 1,
      valorMinimoSorteio: 1,
      valorMaximoSorteio: 8.12,
      separadorDecimal: ',',
      separadorMilhar: '.',
    },
  ],
  validadores: [],
  status: 'naoCalculado',
}

const acessorios6 = {
  variaveis: [],
  familias: [
    {
      id: 'd0d22c31-0ba7-4247-a636-f878df35f756',
      nome: 'F',
      casasDecimais: 2,
      valoresCalculados: [
        {
          id: 'ef6afb12-c1c0-43b9-9a96-infinito',
          nome: 'f1',
          formula: '  ',
        },
      ],
      valoresSorteados: [],
      separacaoMinimaAbsoluta: 1,
      separacaoMinimaRelativa: 1,
      valorMinimoSorteio: 1,
      valorMaximoSorteio: 8.12,
      separadorDecimal: ',',
      separadorMilhar: '.',
    },
  ],
  validadores: [],
  status: 'naoCalculado',
}

const acessorios7 = {
  variaveis: [
    {
      id: '6a9cc7e7-9fff-447b-bba2-36cfb9e798fc',
      nome: 'x',
      tipo: 'padrao',
      casasDecimais: 0,
      intervalos: [
        {
          minimo: 2,
          maximo: 10,
        },
      ],
      avulsos: [],
      separadorDecimal: ',',
      separadorMilhar: '.',
    },
  ],
  familias: [],
  validadores: [
    {
      id: 'ef6afb12-c1c0-43b9-9a96-validador3',
      simbolo: '<=0',
      formula: 'x',
    },
  ],
  status: 'naoCalculado',
}

const acessorios8 = {
  variaveis: [],
  familias: [
    {
      id: 'd0d22c31-0ba7-4247-a636-f878df35f756',
      nome: 'F',
      casasDecimais: 2,
      valoresCalculados: [
        {
          id: 'ef6afb12-c1c0-43b9-9a96-infinito',
          nome: 'f1',
          formula: '2',
        },
      ],
      valoresSorteados: [],
      separacaoMinimaAbsoluta: 1,
      separacaoMinimaRelativa: 1,
      valorMinimoSorteio: 1,
      valorMaximoSorteio: 8.12,
      separadorDecimal: ',',
      separadorMilhar: '.',
    },
  ],
  validadores: [
    {
      id: 'ef6afb12-c1c0-43b9-9a96-validador3',
      simbolo: '<=0',
      formula: 'f1',
    },
  ],
  status: 'naoCalculado',
}

const acessorios9 = {
  variaveis: [
    {
      id: '46943ed4-9ff1-4774-bb69-a6e353f4290a',
      nome: 'x',
      casasDecimais: 0,
      intervalos: [
        {
          minimo: 0,
          maximo: 10,
        },
      ],
      avulsos: [],
      separadorDecimal: ',',
      separadorMilhar: '.',
    },
  ],
  familias: [
    {
      id: 'd0d22c31-0ba7-4247-a636-f878df35f756',
      nome: 'F',
      casasDecimais: 2,
      valoresCalculados: [
        {
          id: 'ef6afb12-c1c0-43b9-9a96-924bdcab5520',
          nome: 'f_1',
          formula: 'cos(x)',
        },
        {
          id: 'ff6afb12-c1c0-43b9-9a96-924bdcab5520',
          nome: 'f_2',
          //formula: 'bisseccao(f(h)=h^2-1 -2 , 2, 4, 3, 4)',
          formula: 'x+1',
        },
      ],
      valoresSorteados: [
        {
          id: '5672e9g0-96fc-40a5-afb5-96d934c235a7',
          nome: 'F_1',
        },
        {
          id: '355dd554-96fc-40a5-afb5-96d934c235a7',
          nome: 'F_2',
        },
      ],
      separacaoMinimaAbsoluta: 1,
      separacaoMinimaRelativa: 1,
      valorMinimoSorteio: 1,
      valorMaximoSorteio: 20,
      separadorDecimal: ',',
      separadorMilhar: '.',
    },
    {
      id: 'e0d22c31-0ba7-4247-a636-f878df35f756',
      nome: 'G',
      casasDecimais: 3,
      valoresCalculados: [
        {
          id: 'gg6afb12-c1c0-43b9-9a96-924bdcab5520',
          nome: 'g_1',
          formula: 'x+F_1',
        },
      ],
      valoresSorteados: [],
      separacaoMinimaAbsoluta: 1,
      separacaoMinimaRelativa: 1,
      valorMinimoSorteio: 0,
      valorMaximoSorteio: 1000,
      separadorDecimal: ',',
      separadorMilhar: '.',
    },
  ],
  validadores: [],
  tipo: 'naoCalculado',
}

const variavelSemIntervalo = {
  variaveis: [
    {
      id: '09882da7-0202-4086-9f7a-50cb27ff8d2c',
      nome: 'x',
      tipo: 'padrao',
      casasDecimais: 0,
      intervalos: [],
      avulsos: [2],
      separadorDecimal: ',',
      separadorMilhar: '',
    },
  ],
  familias: [
    {
      id: '1872ea74-f8b2-4c7f-8311-d842095b8985',
      nome: 'f',
      casasDecimais: 0,
      separadorDecimal: ',',
      separadorMilhar: '',
      separacaoMinimaAbsoluta: 0.1,
      separacaoMinimaRelativa: 10,
      valorMinimoSorteio: 1,
      valorMaximoSorteio: 10,
      valoresSorteados: [
        {
          id: 'a5456acb-c205-4cc1-adf3-d2e62ce5c38b',
          nome: 'F1',
        },
        {
          id: '59053e5d-82c6-4d00-ab5b-5705adf8ea42',
          nome: 'F2',
        },
      ],
      valoresCalculados: [
        {
          id: 'ad3090a0-f2b5-4067-a589-b028e9dea93b',
          nome: 'f1',
          formula: 'x',
        },
      ],
    },
  ],
  validadores: [],
  status: 'naoCalculado',
}

const erroInesperado = {
  variaveis: [
    {
      id: '09882da7-0202-4086-9f7a-50cb27ff8d2c',
      nome: 'x',
      tipo: 'padrao',
      casasDecimais: 'issoEraParaSerUmNumero',
      intervalos: [],
      avulsos: [2],
      separadorDecimal: ',',
      separadorMilhar: '',
    },
  ],
  familias: [],
  validadores: [],
  status: 'naoCalculado',
}

const funcaoTaylorSeno = {
  variaveis: [
    {
      id: '09882da7-0202-4086-9f7a-50cb27ff8d2c',
      nome: 'x',
      tipo: 'padrao',
      casasDecimais: 5,
      intervalos: [],
      avulsos: [3.14159 / 2],
      separadorDecimal: ',',
      separadorMilhar: '',
    },
  ],
  familias: [
    {
      id: '1872ea74-f8b2-4c7f-8311-d842095b8985',
      nome: 'f',
      casasDecimais: 4,
      separadorDecimal: ',',
      separadorMilhar: '',
      separacaoMinimaAbsoluta: null,
      separacaoMinimaRelativa: null,
      valorMinimoSorteio: 1,
      valorMaximoSorteio: 10,
      valoresSorteados: [],
      valoresCalculados: [
        {
          id: 'ad3090a0-f2b5-4067-a589-b028e9dea93b',
          nome: 'f1',
          formula: 'taylorSeno(0,x,4) ',
        },
      ],
    },
  ],
  validadores: [],
  status: 'naoCalculado',
}

const impossivelSortearNeemias = {
  variaveis: [],
  familias: [
    {
      id: '1e8eff33-f4f7-4b2f-9124-c69d1e8820fe',
      nome: 'f',
      casasDecimais: 0,
      separadorDecimal: ',',
      separadorMilhar: '',
      separacaoMinimaAbsoluta: 2,
      separacaoMinimaRelativa: null,
      valorMinimoSorteio: 1,
      valorMaximoSorteio: 6,
      valoresSorteados: [
        {
          id: 'adf4e634-b853-4c16-9f7d-72555ce90726',
          nome: 'F1',
        },
        {
          id: '52152906-fa14-439e-be3b-1c7a3333577b',
          nome: 'F2',
        },
      ],
      valoresCalculados: [ ],
    },
  ],
  validadores: [],
  status: 'naoCalculado',
}

describe('Particularizar questão', () => {
  it('Sorteia mesmo quando não há valores calculados.', () => {
    const resultado = particularizarQuestao(impossivelSortearNeemias)
    expect(resultado.length).toEqual(2)
  })  
  it('Erro na definicao de f1', () => {
    try {
      particularizarQuestao(acessorios4)
    } catch (e) {
      expect(e.errosArray[0].id).toEqual('ef6afb12-c1c0-43b9-9a96-valorNaoDefinido')
    }
  })
  it('Valor de f1 string vazia', () => {
    try {
      particularizarQuestao(acessorios6)
    } catch (e) {
      expect(e.errosArray[0].code).toEqual(codigosErros.funcaoResultadoInvalido)
    }
  })

  it('Valor de f1 infinito', () => {
    try {
      particularizarQuestao(acessorios5)
    } catch (e) {
      expect(e.errosArray[0].id).toEqual('ef6afb12-c1c0-43b9-9a96-infinito')
    }
  })

  it('Validador falha para variáveis', () => {
    try {
      particularizarQuestao(acessorios7)
    } catch (e) {
      expect(e.errosArray[0].id).toEqual('ef6afb12-c1c0-43b9-9a96-validador3')
    }
  })

  it('Validador falha para funções - erro não definitivo', () => {
    try {
      particularizarQuestao(acessorios8)
    } catch (e) {
      expect(e.errosArray[0].id).toEqual('ef6afb12-c1c0-43b9-9a96-validador3')
    }
  })

  it('Gera JSON de sainda corretamente', () => {
    const resultadoParticularizacao = particularizarQuestao(acessorios)
    expect(resultadoParticularizacao[0].id).toEqual('46943ed4-9ff1-4774-bb69-a6e353f4290a')
  })
  it('Sem a variável de quantidade de valores sorteados', () => {
    const resultadoParticularizacao = particularizarQuestao(acessorios2)
    expect(resultadoParticularizacao[0].id).toEqual('6a9cc7e7-9fff-447b-bba2-36cfb9e798fc')
  })
  it('Erro definitivo de validador ', () => {
    try {
      particularizarQuestao(acessorios3)
    } catch (e) {
      expect(e.errosArray[0].id).toEqual('ef6afb12-c1c0-43b9-9a96-validador2')
    }
  })
  it('Teste com valores sorteados dentro de valores calculados', () => {
    const resultadoParticularizacao = particularizarQuestao(acessorios9)
    expect(resultadoParticularizacao.length).toEqual(6)
  })

  it('Variável sem intervalo definido ', () => {
    const res = particularizarQuestao(variavelSemIntervalo)
    expect(res[0].valor).toEqual(2)
  })
  it('Erro desconhecido ', () => {
    try {
      particularizarQuestao(erroInesperado)
    } catch (erro) {
      expect(erro.errosArray[0].code).toEqual(codigosErros.erroDesconhecido)
    }
  })
  it('Função taylorSeno ', () => {
    const res = particularizarQuestao(funcaoTaylorSeno)
    expect(math.round(res[1].valor, 4)).toEqual(0.9248)
  })
})
