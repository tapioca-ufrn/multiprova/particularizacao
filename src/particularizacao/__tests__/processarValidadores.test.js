import {
  processarValidadores,
  processarValidadoresAposVariaveis,
  isErroValidacaoDefinitivo,
} from '../processarValidadores'
import { codigosErros } from '../utils/codigosErros'

const validadores1 = [
  {
    id: 'gg6afb12-c1c0-43b9-9a96-validador1',
    simbolo: '>=0',
    formula: 'x + 1',
  },
  {
    id: 'gg6afb12-c1c0-43b9-9a96-validador2',
    simbolo: '>=0',
    formula: 'x + h + 1',
  },
]
const escopo1 = {
  x: 9,
  h: 2,
}

const validadores2 = [
  {
    id: 'gg6afb12-c1c0-43b9-9a96-validador4',
    simbolo: '<=0',
    formula: 'x + 1',
  },
  {
    id: 'gg6afb12-c1c0-43b9-9a96-validador5',
    simbolo: '>=0',
    formula: 'x + h + 1',
  },
]
const escopo2 = {
  x: 9,
  h: 2,
}
const validadores3 = [
  {
    id: 'gg6afb12-c1c0-43b9-9a96-validador6',
    simbolo: '<=0',
    formula: 'x + 1',
  },
  {
    id: 'gg6afb12-c1c0-43b9-9a96-validador7',
    simbolo: '>=0',
    formula: 'x + h + 1s',
  },
]
const escopo3 = {}
const validadores4 = [
  {
    id: 'gg6afb12-c1c0-43b9-9a96-validador8',
    simbolo: '>=0',
    formula: 'x + 1',
  },
  {
    id: 'gg6afb12-c1c0-43b9-9a96-validador9',
    simbolo: '>=0',
    formula: 'x + h + cosx)',
  },
]
const escopo4 = { x: 9, h: 2 }
const validadores5 = [
  {
    id: 'gg6afb12-c1c0-43b9-9a96-validador10',
    simbolo: '>=0',
    formula: ' ',
  },
  {
    id: 'gg6afb12-c1c0-43b9-9a96-validador11',
    simbolo: '>=0',
    formula: 'x',
  },
]
const escopo5 = { x: 9, h: 2 }

describe('Processamento dos validadores após variáveis', () => {
  it('Sucesso na validação', () => {
    const resultado = processarValidadoresAposVariaveis(validadores1, escopo1)
    expect(resultado).toEqual(true)
  })
  it('Sucesso na validação apesar de variável faltante no escopo', () => {
    const resultado = processarValidadoresAposVariaveis(validadores3, escopo3)
    expect(resultado).toEqual(true)
  })
  it('False na validação', () => {
    try {
      processarValidadoresAposVariaveis(validadores2, escopo2)
    } catch (e) {
      expect(e.errosArray[0].id).toEqual('gg6afb12-c1c0-43b9-9a96-validador4')
    }
  })
  it('Sucesso na validação apesar de receber validador vazio', () => {
    const resultado = processarValidadoresAposVariaveis(validadores5, escopo5)
    expect(resultado).toEqual(true)
  })
})

const erros = [
  {
    code: codigosErros.validadorConteudoInvalido,
  },
]

describe('Processamento dos validadores', () => {
  it('Sucesso na validação', () => {
    const resultado = processarValidadores(validadores1, escopo1)
    expect(resultado).toEqual(true)
  })
  it('Erro na validação', () => {
    try {
      processarValidadores(validadores4, escopo4)
    } catch (e) {
      expect(e.errosArray[0].id).toEqual('gg6afb12-c1c0-43b9-9a96-validador9')
    }
  })
  it('False na validação', () => {
    try {
      processarValidadores(validadores2, escopo2)
    } catch (e) {
      expect(e.errosArray[0].id).toEqual('gg6afb12-c1c0-43b9-9a96-validador4')
    }
  })
  it('Sucesso na validação apesar de receber validador vazio', () => {
    const resultado = processarValidadores(validadores5, escopo5)
    expect(resultado).toEqual(true)
  })
  it('Falha definitiva nos validadores', () => {
    const resultado = isErroValidacaoDefinitivo(erros)
    expect(resultado).toEqual(true)
  })
})
