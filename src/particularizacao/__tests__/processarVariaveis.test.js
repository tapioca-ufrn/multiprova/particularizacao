import { processarVariaveis } from '../processarVariaveis'

const variavelZeroAvulso = [
  {
    id: '46943ed4-9ff1-4774-bb69-a6e353f4290a',
    nome: 'x',
    casasDecimais: 1,
    intervalos: [],
    avulsos: [0],
    separadorDecimal: ',',
    separadorMilhar: '.',
  },
]
describe('processarVariaveis', () => {
  it('Retornava undefined quando 0 era um valor avulso. Agora não mais', () => {
    const resultadoProcessamento = processarVariaveis(variavelZeroAvulso)
    expect(resultadoProcessamento.escopo.x).toEqual(0)
  })
})
