import { sortearValoresFamilia } from '../sortearValoresFamilia'
import { create, all } from 'mathjs'

const math = create(all)

const familia1 = {
  id: 'd0d22c31-0ba7-4247-a636-f878df35f756',
  nome: 'F',
  casasDecimais: 0,
  quantidadeValoresSorteados: 4,
  valoresCalculados: [],
  valoresSorteados: [],
  separacaoMinimaAbsoluta: 1,
  separacaoMinimaRelativa: 0,
  valorMinimoSorteio: 0,
  valorMaximoSorteio: 3,
  separadorDecimal: ',',
  separadorMilhar: '.',
}
const valoresCalculados1 = []

const familia2 = {
  id: 'd0d22c31-0ba7-4247-a636-f878df35f756',
  nome: 'F',
  casasDecimais: 0,
  quantidadeValoresSorteados: 5,
  valoresCalculados: [],
  valoresSorteados: [],
  separacaoMinimaAbsoluta: 1,
  separacaoMinimaRelativa: 0,
  valorMinimoSorteio: 0,
  valorMaximoSorteio: 3,
  separadorDecimal: ',',
  separadorMilhar: '.',
}
const valoresCalculados2 = []

const familia5 = {
  id: 'd0d22c31-0ba7-4247-a636-f878df35f756',
  nome: 'F',
  casasDecimais: 1,
  quantidadeValoresSorteados: 30,
  valoresCalculados: [],
  valoresSorteados: [],
  separacaoMinimaAbsoluta: 0.2,
  separacaoMinimaRelativa: 0,
  valorMinimoSorteio: null,
  valorMaximoSorteio: 140,
  separadorDecimal: ',',
  separadorMilhar: '.',
}
const valoresCalculados5 = [1, 150.0]

const familia5a = {
  id: 'd0d22c31-0ba7-4247-a636-f878df35f756',
  nome: 'F',
  casasDecimais: 1,
  quantidadeValoresSorteados: 30,
  valoresCalculados: [],
  valoresSorteados: [],
  separacaoMinimaAbsoluta: 0.2,
  separacaoMinimaRelativa: 0,
  valorMinimoSorteio: null,
  valorMaximoSorteio: 20,
  separadorDecimal: ',',
  separadorMilhar: '.',
}
const valoresCalculados5a = [70, 150.0]

const familia6 = {
  id: 'd0d22c31-0ba7-4247-a636-f878df35f756',
  nome: 'F',
  casasDecimais: 1,
  quantidadeValoresSorteados: 30,
  valoresCalculados: [],
  valoresSorteados: [],
  separacaoMinimaAbsoluta: 0.2,
  separacaoMinimaRelativa: 0,
  valorMinimoSorteio: 40,
  valorMaximoSorteio: null,
  separadorDecimal: ',',
  separadorMilhar: '.',
}
const valoresCalculados6 = [1, 150.0]

const familia6a = {
  id: 'd0d22c31-0ba7-4247-a636-f878df35f756',
  nome: 'F',
  casasDecimais: 1,
  quantidadeValoresSorteados: 30,
  valoresCalculados: [],
  valoresSorteados: [],
  separacaoMinimaAbsoluta: 0.2,
  separacaoMinimaRelativa: 0,
  valorMinimoSorteio: 140,
  valorMaximoSorteio: null,
  separadorDecimal: ',',
  separadorMilhar: '.',
}
const valoresCalculados6a = [1, 20]

const familia7 = {
  id: 'd0d22c31-0ba7-4247-a636-f878df35f756',
  nome: 'F',
  casasDecimais: 1,
  quantidadeValoresSorteados: 30,
  valoresCalculados: [],
  valoresSorteados: [],
  separacaoMinimaAbsoluta: 0.2,
  separacaoMinimaRelativa: 0,
  valorMinimoSorteio: null,
  valorMaximoSorteio: null,
  separadorDecimal: ',',
  separadorMilhar: '.',
}
const valoresCalculados7 = [-50, 150.0]

const familia10 = {
  id: 'd0d22c31-0ba7-4247-a636-f878df35f756',
  nome: 'F',
  casasDecimais: 1,
  quantidadeValoresSorteados: 2,
  valoresCalculados: [],
  valoresSorteados: [],
  separacaoMinimaAbsoluta: 2,
  separacaoMinimaRelativa: 1,
  valorMinimoSorteio: null,
  valorMaximoSorteio: null,
  separadorDecimal: ',',
  separadorMilhar: '.',
}
const valoresCalculados10 = [-10.32]

const sorteiosCalculadosValoresProximos11 = {
  id: '3467bb5a-49f5-45b8-aaf5-ca4d94469f4c',
  nome: 'f',
  casasDecimais: 0,
  separadorDecimal: ',',
  separadorMilhar: '',
  separacaoMinimaAbsoluta: 0,
  separacaoMinimaRelativa: 0,
  valorMinimoSorteio: 0,
  valorMaximoSorteio: 5,
  quantidadeValoresSorteados: 6,
  valoresCalculados: [],
  valoresSorteados: [],
}

const valoresCalculados11 = [0]

const erroPossibilidades = {
  id: '81985a44-5451-4451-88ba-4768fd75de53',
  nome: 'f',
  casasDecimais: 1,
  separadorDecimal: ',',
  separadorMilhar: '',
  separacaoMinimaAbsoluta: 0.2,
  separacaoMinimaRelativa: 0,
  valorMinimoSorteio: 2,
  valorMaximoSorteio: 4,
  valoresSorteados: [],
  valoresCalculados: [],
  quantidadeValoresSorteados: 10,
}
const erroPossibilidadesVCalculados = [3.2]

const estavaSorteandoForaDoIntervalo = {
  id: '81985a44-5451-4451-88ba-4768fd75de51',
  nome: 'f',
  casasDecimais: 2,
  separadorDecimal: ',',
  separadorMilhar: '',
  separacaoMinimaAbsoluta: 0.2,
  separacaoMinimaRelativa: 0,
  valorMinimoSorteio: 0.3,
  valorMaximoSorteio: 2.2,
  valoresSorteados: [],
  valoresCalculados: [],
  quantidadeValoresSorteados: 9,
}
const estavaSorteandoForaDoIntervaloValoresCalculados = [0.15]

describe('Sortear valores de funções', () => {
  it('Só tem uma opção de sorteio. Sorteio OK. Deu erro na prova de Neemias', () => {
    const resultado = sortearValoresFamilia(erroPossibilidades, erroPossibilidadesVCalculados)
    expect(resultado.length).toEqual(10)
  })

  it('Só tem uma opção de sortear: 0, 1,2 e 3', () => {
    const resuldoSorteio = sortearValoresFamilia(familia1, valoresCalculados1)
    expect(resuldoSorteio[0]).toEqual(0)
    expect(resuldoSorteio[3]).toEqual(3)
  })

  it('Não é possível sortear', () => {
    try {
      sortearValoresFamilia(familia2, valoresCalculados2)
    } catch (erro) {
      expect(erro.code).toEqual('impossivel_sortear_valores_para_a_familia')
    }
  })

  it('Valores sorteados devem ficar abaixo do valor máximo', () => {
    const resuldoSorteio = sortearValoresFamilia(familia5, valoresCalculados5)
    expect(math.max(resuldoSorteio)).toBeLessThanOrEqual(140)
  })

  it('Valores sorteados devem ficar abaixo do valor máximo - Parte 2', () => {
    const resuldoSorteio = sortearValoresFamilia(familia5a, valoresCalculados5a)
    expect(math.max(resuldoSorteio)).toBeLessThanOrEqual(20)
  })

  it('Valores sorteados devem ficar acima do valor mínimo', () => {
    const resuldoSorteio = sortearValoresFamilia(familia6, valoresCalculados6)
    expect(math.min(resuldoSorteio)).toBeGreaterThanOrEqual(40)
  })

  it('Valores sorteados devem ficar acima do valor mínimo - parte 2', () => {
    const resuldoSorteio = sortearValoresFamilia(familia6a, valoresCalculados6a)
    expect(math.min(resuldoSorteio)).toBeGreaterThanOrEqual(140)
  })

  it('Valores sorteados devem ficar no intervalo', () => {
    const resuldoSorteio = sortearValoresFamilia(familia7, valoresCalculados7)
    expect(math.max(resuldoSorteio)).toBeLessThanOrEqual(1050)
    expect(math.min(resuldoSorteio)).toBeGreaterThanOrEqual(-1050)
  })

  it('Verificar isso', () => {
    const resuldoSorteio = sortearValoresFamilia(familia10, valoresCalculados10)
    expect(math.max(resuldoSorteio)).toBeLessThanOrEqual(1050)
    expect(math.min(resuldoSorteio)).toBeGreaterThanOrEqual(-1050)
  })
  it('Valores calculados na borda - impossível sortear', () => {
    try {
      sortearValoresFamilia(sorteiosCalculadosValoresProximos11, valoresCalculados11)
    } catch (erro) {
      expect(erro.code).toEqual('impossivel_sortear_valores_para_a_familia')
    }
  })

  it('Estava sorteando valores fora do intervalo', () => {
    const resuldoSorteio = sortearValoresFamilia(
      estavaSorteandoForaDoIntervalo,
      estavaSorteandoForaDoIntervaloValoresCalculados,
    )
    expect(math.max(resuldoSorteio)).toBeLessThanOrEqual(2.2001)
    expect(math.min(resuldoSorteio)).toBeGreaterThanOrEqual(0.2999)
  })
})
