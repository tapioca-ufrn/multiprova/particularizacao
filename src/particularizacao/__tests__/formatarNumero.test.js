import { formatarNumero } from '../formatarNumero'
import { create, all } from 'mathjs'

const math = create(all)

describe('Verificar formatação de números', () => {
  it('Com inteiros -  verficar valor', () => {
    const resultado = formatarNumero(2, 2, ',', '')
    expect(resultado.valor).toEqual(2)
  })
  it('Com inteiros -  verficar valorFormatado', () => {
    const resultado = formatarNumero(2, 2, ',', '')
    expect(resultado.valorFormatado).toEqual('2,00')
  })
  it('Números grandes -  verficar valorFormatado', () => {
    const resultado = formatarNumero(-23234.3355, 3, ',', '.')
    expect(resultado.valorFormatado).toEqual('-23.234,336')
  })
  it('Números grandes interos -  verficar valorFormatado', () => {
    const resultado = formatarNumero(23234, 3, ',', '.')
    expect(resultado.valorFormatado).toEqual('23.234,000')
  })
  it('Números grandes interos -  verficar valor', () => {
    const resultado = formatarNumero(-23234, 3, ',', '.')
    expect(resultado.valor).toEqual(-23234)
  })
})

describe('Verificar formatação de complexos', () => {
  it('Partes inteiras. Verifica formatação', () => {
    const n = math.complex(2, 4)
    const resultado = formatarNumero(n, 2, ',', '')
    expect(resultado.valorFormatado).toEqual('2,00+4,00i')
  })
  it('Partes racionais. Verifica formatação', () => {
    const n = math.complex(2.23423, 4.455)
    const resultado = formatarNumero(n, 2, ',', '')
    expect(resultado.valorFormatado).toEqual('2,23+4,46i')
  })
  it('Somente real. Verifica formatação', () => {
    const n = math.complex(2.23423, 0)
    const resultado = formatarNumero(n, 2, ',', '')
    expect(resultado.valorFormatado).toEqual('2,23')
  })
  it('Somente real negativo. Verifica formatação', () => {
    const n = math.complex(-2.23423, 0)
    const resultado = formatarNumero(n, 2, ',', '')
    expect(resultado.valorFormatado).toEqual('-2,23')
  })
  it('Somente imaginária negativa. Verifica formatação', () => {
    const n = math.complex(0, -2.23423)
    const resultado = formatarNumero(n, 2, ',', '')
    expect(resultado.valorFormatado).toEqual('-2,23i')
  })
  it('Somente imaginária positiva. Verifica formatação', () => {
    const n = math.complex(0, 2.23423)
    const resultado = formatarNumero(n, 2, ',', '')
    expect(resultado.valorFormatado).toEqual('2,23i')
  })
  it('Complexo igual a zero', () => {
    const n = math.complex(0.00044, 0.0000023423)
    const resultado = formatarNumero(n, 2, ',', '')
    expect(resultado.valorFormatado).toEqual('0')
  })
})

describe('Verificar formatação de matrizes', () => {
  it('Formata Array com 2 casas decimais sem separador de milhar', () => {
    const array = [
      [2.333, 3.555],
      [4444.999, 1234],
    ]
    const formatado = formatarNumero(array, 2, ',', '')
    expect(formatado.valorFormatado).toEqual('\\begin{bmatrix}2,33&3,56\\\\4445,00&1234,00\\\\\\end{bmatrix}')
  })
  it('Formata Array com 2 casas decimais com separador de milhar', () => {
    const array = [
      [2.333, 3.555],
      [4444.999, 1234],
    ]
    const formatado = formatarNumero(array, 2, ',', '.')
    expect(formatado.valorFormatado).toEqual('\\begin{bmatrix}2,33&3,56\\\\4.445,00&1.234,00\\\\\\end{bmatrix}')
  })
  it('Formata Array com 0 casas decimais com separador de milhar', () => {
    const array = [
      [2.333, 3.555],
      [4444.999, 1234],
    ]
    const formatado = formatarNumero(array, 0, ',', '.')
    expect(formatado.valorFormatado).toEqual('\\begin{bmatrix}2&4\\\\4.445&1.234\\\\\\end{bmatrix}')
  })
  it('Formata Matrix com 2 casas decimais com separador de milhar', () => {
    const array = [
      [2.333, 3.555],
      [4444.999, 1234],
    ]
    const matrix = math.matrix(array)
    const formatado = formatarNumero(matrix, 2, ',', '.')
    expect(formatado.valorFormatado).toEqual('\\begin{bmatrix}2,33&3,56\\\\4.445,00&1.234,00\\\\\\end{bmatrix}')
  })
  it('Formata Matrix com números negativos, 2 casas decimais com separador de milhar', () => {
    const array = [
      [-2.333, 3.555],
      [-4444.999, -1234],
    ]
    const matrix = math.matrix(array)
    const formatado = formatarNumero(matrix, 2, ',', '.')
    expect(formatado.valorFormatado).toEqual('\\begin{bmatrix}-2,33&3,56\\\\-4.445,00&-1.234,00\\\\\\end{bmatrix}')
  })
})
