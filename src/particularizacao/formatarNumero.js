import { tiposMathjs } from './utils/tiposMathjs'
import { create, all } from 'mathjs'
const math = create(all)

const formataNumero = (num, casasDecimais, separadorDecimal, separadorMilhar) => {
  const numeroArredondado = math.round(num, casasDecimais)
  const numero = numeroArredondado.toFixed(casasDecimais)

  let inteiroDecimal = numero.toString().split('.')
  inteiroDecimal[0] = inteiroDecimal[0].replace(/\B(?=(\d{3})+(?!\d))/g, separadorMilhar)
  const numeroFormatado = inteiroDecimal.join(separadorDecimal)

  return { valor: numeroArredondado, valorFormatado: numeroFormatado, latex: false }
}

const formataMatriz = (matriz, casasDecimais, separadorDecimal, separadorMilhar) => {
  const matrizArredondada = math.round(matriz, casasDecimais)

  let matrizFormatada = matrizArredondada.map((value, index, matrix) => {
    const valorFormatato = formataNumero(value, casasDecimais, separadorDecimal, separadorMilhar)
    return valorFormatato.valorFormatado
  })

  matrizFormatada = math.parse(String(matrizFormatada))
  matrizFormatada = matrizFormatada.toTex()

  let expReg = new RegExp('\\\\mathtt{"([-0-9' + separadorMilhar + ']*\\' + separadorDecimal + '[0-9]*)"}', 'g')
  if (casasDecimais === 0) {
    expReg = new RegExp('\\\\mathtt{"([-0-9' + separadorMilhar + ']*)"}', 'g')
  }
  matrizFormatada = matrizFormatada.replace(expReg, '$1')

  return { valor: matrizArredondada, valorFormatado: matrizFormatada, latex: true }
}

const formataArray = (qtde, casasDecimais, separadorDecimal, separadorMilhar) => {
  const matriz = math.matrix(qtde)
  return formataMatriz(matriz, casasDecimais, separadorDecimal, separadorMilhar)
}

const formataComplexo = (qtde, casasDecimais, separadorDecimal, separadorMilhar) => {
  const numeroArredondado = math.round(qtde, casasDecimais)
  const reFormatada = formataNumero(qtde.re, casasDecimais, separadorDecimal, separadorMilhar)
  const imFormatada = formataNumero(qtde.im, casasDecimais, separadorDecimal, separadorMilhar)

  let numeroFormatado = ''
  if (numeroArredondado.re) {
    numeroFormatado = reFormatada.valorFormatado
    if (numeroArredondado.im > 0) numeroFormatado += '+'
  } else if (!numeroArredondado.im) {
    numeroFormatado = '0'
  }
  if (numeroArredondado.im) {
    numeroFormatado += imFormatada.valorFormatado + 'i'
  }

  return { valor: numeroArredondado, valorFormatado: numeroFormatado, latex: false }
}

export const formatarNumero = (qtde, casasDecimais, separadorDecimal, separadorMilhar) => {
  const tipo = math.typeOf(qtde)
  let saida

  switch (tipo) {
    case tiposMathjs.array:
    case tiposMathjs.matrix:
      saida = formataArray(qtde, casasDecimais, separadorDecimal, separadorMilhar)
      break
    case tiposMathjs.complex:
      saida = formataComplexo(qtde, casasDecimais, separadorDecimal, separadorMilhar)
      break
    case tiposMathjs.number:
    case tiposMathjs.bigNumber:
      saida = formataNumero(qtde, casasDecimais, separadorDecimal, separadorMilhar)
      break
  }
  return saida
}
