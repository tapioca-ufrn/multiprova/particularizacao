import { formatarNumero } from './formatarNumero'
import { getRandomInt } from './utils/getRanInt'
import { create, all } from 'mathjs'

const math = create(all)

const organizeDominioParaSorteioVariaveis = (variavel) => {
  const { casasDecimais, minimo, maximo, avulsos } = variavel

  const separacaoDecimal = math.pow(10, -casasDecimais)

  let numeroDePossibilidadesDentroIntervalo = 0
  if (minimo != null && maximo != null) {
    numeroDePossibilidadesDentroIntervalo = math.floor((maximo - minimo) / separacaoDecimal) + 1
  }

  let avulsosForaIntervalo = []

  for (const avulso of avulsos) {
    if (avulso < minimo || avulso > maximo || minimo == null || maximo == null) {
      avulsosForaIntervalo.push(avulso)
    }
  }

  const numeroAvulsosForaIntervalo = avulsosForaIntervalo.length
  const numeroTotalPossibilidadesParaSorteio = numeroDePossibilidadesDentroIntervalo + numeroAvulsosForaIntervalo

  return {
    numeroDePossibilidadesDentroIntervalo: numeroDePossibilidadesDentroIntervalo,
    numeroTotalPossibilidadesParaSorteio: numeroTotalPossibilidadesParaSorteio,
    avulsosForaIntervalo: avulsosForaIntervalo,
  }
}

const sortear = (variavel) => {
  const {
    casasDecimais,
    minimo,
    numeroDePossibilidadesDentroIntervalo,
    numeroTotalPossibilidadesParaSorteio,
    avulsosForaIntervalo,
  } = variavel

  const rotuloAleatorio = getRandomInt(1, numeroTotalPossibilidadesParaSorteio)

  const separacaoDecimal = math.pow(10, -casasDecimais)

  let valorSorteado
  if (rotuloAleatorio <= numeroDePossibilidadesDentroIntervalo) {
    const indiceRotuloAleatorio = rotuloAleatorio - 1
    valorSorteado = minimo + indiceRotuloAleatorio * separacaoDecimal
  } else {
    const indiceRotuloAleatorio = rotuloAleatorio - 1 - numeroDePossibilidadesDentroIntervalo
    valorSorteado = avulsosForaIntervalo[indiceRotuloAleatorio]
  }
  return valorSorteado
}

const sortearVariavel = (variavel) => {
  variavel = { ...variavel, ...organizeDominioParaSorteioVariaveis(variavel) }
  const valorSorteado = sortear(variavel)

  return {
    id: variavel.id,
    nome: variavel.nome,
    tipo: 'variavelPadrao',
    ...formatarNumero(valorSorteado, variavel.casasDecimais, variavel.separadorDecimal, variavel.separadorMilhar),
  }
}

const montarEscopo = (resultados) => {
  let escopo = {}
  for (const resultado of resultados) {
    escopo = { ...escopo, [resultado.nome]: resultado.valor }
  }
  return escopo
}

const arredondamentoInicial = (variavel) => {
  variavel.minimo = variavel.intervalos.length
    ? math.round(variavel.intervalos[0].minimo, variavel.casasDecimais)
    : null
  variavel.maximo = variavel.intervalos.length
    ? math.round(variavel.intervalos[0].maximo, variavel.casasDecimais)
    : null

  for (let i = 0; i < variavel.avulsos.length; i++) {
    variavel.avulsos[i] = math.round(variavel.avulsos[i], variavel.casasDecimais)
  }
  return variavel
}

export const processarVariaveis = (variaveis) => {
  let resultadoVariaveis = []

  for (let variavel of variaveis) {
    variavel = arredondamentoInicial(variavel)
    resultadoVariaveis.push(sortearVariavel(variavel))
  }

  return { escopo: montarEscopo(resultadoVariaveis), resultado: resultadoVariaveis }
}
