import { getRandomInt } from './utils/getRanInt'
import { definirSeparacaoMinima } from './definirSeparacaoMinima'
import { codigosErros } from './utils/codigosErros'
import { ParticularizaError } from './utils/ParticularizaError'

import { create, all } from 'mathjs'

const math = create(all)

const atualizaBordas = (
  bordas,
  novoValorDeFuncaoSorteado,
  indiceDaRegiaoSorteada,
  separacaoMinima,
  separacaoDecimal,
  casasDecimais,
) => {
  const bordaSorteada = bordas[indiceDaRegiaoSorteada]
  const extremoInferiorRegiaoSorteada = bordaSorteada.minimo
  const extremoSuperiorRegiaoSorteada = bordaSorteada.maximo

  let temp1 = math.round(extremoSuperiorRegiaoSorteada - novoValorDeFuncaoSorteado, casasDecimais)
  let temp2 = math.round(novoValorDeFuncaoSorteado - extremoInferiorRegiaoSorteada, casasDecimais)
  if (temp1 >= separacaoMinima && temp2 < separacaoMinima) {
    bordas.push({
      minimo: math.round(novoValorDeFuncaoSorteado + separacaoMinima, casasDecimais),
      maximo: extremoSuperiorRegiaoSorteada,
      capacidadeSorteio:
        Math.floor(
          math.round(
            (extremoSuperiorRegiaoSorteada - (novoValorDeFuncaoSorteado + separacaoMinima)) / separacaoMinima,
            casasDecimais + 1,
          ),
        ) + 1,
      numeroPossibilidades:
        Math.floor(
          math.round(
            (extremoSuperiorRegiaoSorteada - (novoValorDeFuncaoSorteado + separacaoMinima)) / separacaoDecimal,
            casasDecimais,
          ),
        ) + 1,
    })
  }

  temp1 = math.round(extremoSuperiorRegiaoSorteada - novoValorDeFuncaoSorteado, casasDecimais)
  temp2 = math.round(novoValorDeFuncaoSorteado - extremoInferiorRegiaoSorteada, casasDecimais)
  if (temp1 < separacaoMinima && temp2 >= separacaoMinima) {
    bordas.push({
      minimo: extremoInferiorRegiaoSorteada,
      maximo: math.round(novoValorDeFuncaoSorteado - separacaoMinima, casasDecimais),
      capacidadeSorteio:
        Math.floor(
          math.round(
            (novoValorDeFuncaoSorteado - separacaoMinima - extremoInferiorRegiaoSorteada) / separacaoMinima,
            casasDecimais + 1,
          ),
        ) + 1,
      numeroPossibilidades:
        Math.floor(
          math.round(
            (novoValorDeFuncaoSorteado - separacaoMinima - extremoInferiorRegiaoSorteada) / separacaoDecimal,
            casasDecimais,
          ),
        ) + 1,
    })
  }

  temp1 = math.round(extremoSuperiorRegiaoSorteada - novoValorDeFuncaoSorteado, casasDecimais)
  temp2 = math.round(novoValorDeFuncaoSorteado - extremoInferiorRegiaoSorteada, casasDecimais)
  if (temp1 >= separacaoMinima && temp2 >= separacaoMinima) {
    bordas.push({
      minimo: math.round(novoValorDeFuncaoSorteado + separacaoMinima, casasDecimais),
      maximo: extremoSuperiorRegiaoSorteada,
      capacidadeSorteio:
        Math.floor(
          math.round(
            (extremoSuperiorRegiaoSorteada - (novoValorDeFuncaoSorteado + separacaoMinima)) / separacaoMinima,
            casasDecimais + 1,
          ),
        ) + 1,
      numeroPossibilidades:
        Math.floor(
          math.round(
            (extremoSuperiorRegiaoSorteada - (novoValorDeFuncaoSorteado + separacaoMinima)) / separacaoDecimal,
            casasDecimais,
          ),
        ) + 1,
    })
    bordas.push({
      minimo: extremoInferiorRegiaoSorteada,
      maximo: math.round(novoValorDeFuncaoSorteado - separacaoMinima, casasDecimais),
      capacidadeSorteio:
        Math.floor(
          math.round(
            (novoValorDeFuncaoSorteado - separacaoMinima - extremoInferiorRegiaoSorteada) / separacaoMinima,
            casasDecimais + 1,
          ),
        ) + 1,
      numeroPossibilidades:
        Math.floor(
          math.round(
            (novoValorDeFuncaoSorteado - separacaoMinima - extremoInferiorRegiaoSorteada) / separacaoDecimal,
            casasDecimais,
          ),
        ) + 1,
    })
  }

  bordas.splice(indiceDaRegiaoSorteada, 1)
  return bordas
}

const obterSitioAleatorio = (regiaoSorteada, maximoDesperdicioDeValoresSorteados, separacaoMinima, casasDecimais) => {
  const indiceUltimoSitioNaRegiaoSorteada = regiaoSorteada.numeroPossibilidades - 1

  let sitioSorteadoNaRegiaoSorteada = 0
  if (maximoDesperdicioDeValoresSorteados > 0) {
    sitioSorteadoNaRegiaoSorteada = getRandomInt(0, indiceUltimoSitioNaRegiaoSorteada)
  } else {
    const t1 = math.round((regiaoSorteada.maximo - regiaoSorteada.minimo) * math.pow(10, casasDecimais), 0)
    const t2 = math.round(separacaoMinima * math.pow(10, casasDecimais), 0)
    const sitioMaximo = math.round(math.mod(t1, t2), 0)
    sitioSorteadoNaRegiaoSorteada = getRandomInt(0, sitioMaximo)
  }

  return sitioSorteadoNaRegiaoSorteada
}

const obterQuantidadeMaximaDeValoresPermitidos = (bordas) => {
  let capacidadeTotal = 0

  for (const borda of bordas) {
    capacidadeTotal += borda.capacidadeSorteio
  }
  return capacidadeTotal
}

const obterBordas = (valoresInternos, cotaInferior, cotaSuperior, separacaoDecimal, separacaoMinima, casasDecimais) => {
  const bordas = []

  valoresInternos = math.sort(valoresInternos)

  if (math.round(valoresInternos[0] - separacaoMinima, casasDecimais) >= cotaInferior) {
    bordas.push({
      minimo: cotaInferior,
      maximo: math.round(valoresInternos[0] - separacaoMinima, casasDecimais),
      capacidadeSorteio:
        Math.floor(
          math.round((valoresInternos[0] - separacaoMinima - cotaInferior) / separacaoMinima, casasDecimais + 1),
        ) + 1,
      numeroPossibilidades:
        Math.floor(
          math.round((valoresInternos[0] - separacaoMinima - cotaInferior) / separacaoDecimal, casasDecimais),
        ) + 1,
    })
  }
  for (let i = 0; i < valoresInternos.length - 1; i++) {
    const temp1 = math.round(valoresInternos[i] + separacaoMinima, casasDecimais)
    const temp2 = math.round(valoresInternos[i + 1] - separacaoMinima, casasDecimais)
    if (temp1 <= temp2) {
      bordas.push({
        minimo: math.round(valoresInternos[i] + separacaoMinima, casasDecimais),
        maximo: math.round(valoresInternos[i + 1] - separacaoMinima, casasDecimais),
        capacidadeSorteio:
          Math.floor(
            math.round(
              (valoresInternos[i + 1] - separacaoMinima - (valoresInternos[i] + separacaoMinima)) / separacaoMinima,
              casasDecimais + 1,
            ),
          ) + 1,
        numeroPossibilidades:
          Math.floor(
            math.round(
              (valoresInternos[i + 1] - separacaoMinima - (valoresInternos[i] + separacaoMinima)) / separacaoDecimal,
              casasDecimais,
            ),
          ) + 1,
      })
    }
  }

  if (math.round(valoresInternos[valoresInternos.length - 1] + separacaoMinima, casasDecimais) <= cotaSuperior) {
    bordas.push({
      minimo: math.round(valoresInternos[valoresInternos.length - 1] + separacaoMinima, casasDecimais),
      maximo: cotaSuperior,
      capacidadeSorteio:
        Math.floor(
          math.round(
            (cotaSuperior - (valoresInternos[valoresInternos.length - 1] + separacaoMinima)) / separacaoMinima,
            casasDecimais + 1,
          ),
        ) + 1,
      numeroPossibilidades:
        Math.floor(
          math.round(
            (cotaSuperior - (valoresInternos[valoresInternos.length - 1] + separacaoMinima)) / separacaoDecimal,
            casasDecimais,
          ),
        ) + 1,
    })
  }

  return bordas
}

const obterBordasIniciaisDasRegioesDeValoresPermitidos = (
  valoresCalculadosDentroIntervalo,
  intervalo,
  separacaoDecimal,
  separacaoMinima,
  casasDecimais,
) => {
  const { minimo, maximo } = intervalo

  let bordas = []

  if (valoresCalculadosDentroIntervalo.length > 0) {
    bordas = bordas.concat(
      obterBordas(valoresCalculadosDentroIntervalo, minimo, maximo, separacaoDecimal, separacaoMinima, casasDecimais),
    )
  } else {
    bordas.push({
      minimo: minimo,
      maximo: maximo,
      capacidadeSorteio: Math.floor((maximo - minimo) / separacaoMinima) + 1,
      numeroPossibilidades: Math.floor((maximo - minimo) / separacaoDecimal) + 1,
    })
  }

  return bordas
}

const obterValoresCalculadosDentroIntervalo = (valores, intervalo) => {
  const { minimo, maximo } = intervalo
  const valoresCalculadosDentroIntervalo = []

  for (let i = 0; i < valores.length; i++) {
    if (valores[i] >= minimo && valores[i] <= maximo) {
      valoresCalculadosDentroIntervalo.push(valores[i])
    }
  }
  return valoresCalculadosDentroIntervalo
}

const obterValoresMaioresMinimoInput = (minimo, valores) => {
  const valoresMaioresMinimo = []
  for (const valor of valores) {
    if (valor >= minimo) {
      valoresMaioresMinimo.push(valor)
    }
  }
  return valoresMaioresMinimo
}

const obterValoresMenoresMaximoInput = (maximo, valores) => {
  const valoresMenoresMaximo = []
  for (const valor of valores) {
    if (valor <= maximo) {
      valoresMenoresMaximo.push(valor)
    }
  }
  return valoresMenoresMaximo
}

const obterIntervaloParaSorteioFuncoes = (
  valores,
  minimo,
  maximo,
  casasDecimais,
  tamanhoDeReferencia,
  forcaMesmoSinal,
) => {
  let intervalo

  if (minimo == null && maximo == null) {
    intervalo = {
      minimo: math.min(valores) - math.round(tamanhoDeReferencia, casasDecimais),
      maximo: math.max(valores) + math.round(tamanhoDeReferencia, casasDecimais),
    }
  }
  if (minimo != null && maximo != null) {
    // se existirem valores calculados, então obter valoresInternos =
    const valoresInternos = obterValoresCalculadosDentroIntervalo(valores, { minimo: minimo, maximo: maximo })
    if (valoresInternos.length === 0) {
      intervalo = { minimo: minimo, maximo: maximo }
    } else {
      intervalo = {
        minimo: math.min(valoresInternos) - math.round(tamanhoDeReferencia, casasDecimais),
        maximo: math.max(valoresInternos) + math.round(tamanhoDeReferencia, casasDecimais),
      }
      intervalo.minimo = math.max(intervalo.minimo, minimo)
      intervalo.maximo = math.min(intervalo.maximo, maximo)
    }
  }
  if (minimo != null && maximo == null) {
    const valoresMaioresMinimo = obterValoresMaioresMinimoInput(minimo, valores)
    if (valoresMaioresMinimo.length === 0) {
      intervalo = { minimo: minimo, maximo: minimo + tamanhoDeReferencia }
    } else {
      intervalo = {
        minimo: math.min(valoresMaioresMinimo) - math.round(tamanhoDeReferencia, casasDecimais),
        maximo: math.max(valoresMaioresMinimo) + math.round(tamanhoDeReferencia, casasDecimais),
      }
      intervalo.minimo = math.max(intervalo.minimo, minimo)
    }
  }
  if (minimo == null && maximo != null) {
    const valoresMenoresMaximo = obterValoresMenoresMaximoInput(maximo, valores)
    if (valoresMenoresMaximo.length === 0) {
      intervalo = { minimo: maximo - tamanhoDeReferencia, maximo: maximo }
    } else {
      intervalo = {
        minimo: math.min(valoresMenoresMaximo) - math.round(tamanhoDeReferencia, casasDecimais),
        maximo: math.max(valoresMenoresMaximo) + math.round(tamanhoDeReferencia, casasDecimais),
      }
      intervalo.maximo = math.min(intervalo.maximo, maximo)
    }
  }

  if (forcaMesmoSinal && minimo == null && maximo == null) {
    if (math.max(valores) * math.min(valores) > 0) {
      if (math.max(valores) < 0) {
        intervalo.maximo = math.min(intervalo.maximo, 0)
      }
      if (math.min(valores) > 0) {
        intervalo.minimo = math.max(intervalo.minimo, 0)
      }
    }
  }
  return intervalo
}

const obterTamanhoDeReferencia = (valores, separacaoMinima, valorMinimoSorteio, valorMaximoSorteio, casasDecimais) => {
  let tamanhoDeReferencia
  if (valores.length === 0) {
    tamanhoDeReferencia = valorMaximoSorteio - valorMinimoSorteio
  } else {
    let amplitude = math.max(valores) - math.min(valores)
    if (valores.length === 1) {
      amplitude = math.abs(valores[0])
    }
    tamanhoDeReferencia = math.round(
      0.1 * getRandomInt(5, 10) * math.max(15 * separacaoMinima, 3 * amplitude),
      casasDecimais,
    )
  }

  return tamanhoDeReferencia
}

export const sortearValoresFamilia = (familia, valoresCalculadosArredondados) => {
  const {
    id,
    casasDecimais,
    quantidadeValoresSorteados,
    valorMinimoSorteio,
    valorMaximoSorteio,
    separacaoMinimaAbsoluta,
    separacaoMinimaRelativa,
  } = familia

  let quantidadeValoresDemandada = quantidadeValoresSorteados

  const forcaMesmoSinal = true

  const separacaoDecimal = Math.pow(10, -casasDecimais)

  const separacaoMinima = definirSeparacaoMinima(
    separacaoMinimaAbsoluta,
    separacaoMinimaRelativa,
    casasDecimais,
    valorMaximoSorteio,
    valorMinimoSorteio,
    valoresCalculadosArredondados,
  )

  const tamanhoDeReferencia = obterTamanhoDeReferencia(
    valoresCalculadosArredondados,
    separacaoMinima,
    valorMinimoSorteio,
    valorMaximoSorteio,
    casasDecimais,
  )

  let intervalo = obterIntervaloParaSorteioFuncoes(
    valoresCalculadosArredondados,
    valorMinimoSorteio,
    valorMaximoSorteio,
    casasDecimais,
    tamanhoDeReferencia,
    forcaMesmoSinal,
  )

  const valoresCalculadosDentroIntervalo = obterValoresCalculadosDentroIntervalo(
    valoresCalculadosArredondados,
    intervalo,
  )

  let bordas = obterBordasIniciaisDasRegioesDeValoresPermitidos(
    valoresCalculadosDentroIntervalo,
    intervalo,
    separacaoDecimal,
    separacaoMinima,
    casasDecimais,
  )

  let quantidadeMaximaDeValoresPermitidos = obterQuantidadeMaximaDeValoresPermitidos(bordas)
  let maximoDesperdicioDeValoresSorteados = quantidadeMaximaDeValoresPermitidos - quantidadeValoresDemandada
  const valoresSorteadosDeFuncoes = []

  if (quantidadeMaximaDeValoresPermitidos >= quantidadeValoresSorteados) {
    for (let indice = 0; indice < quantidadeValoresSorteados; indice++) {
      const indiceDaRegiaoSorteada = getRandomInt(0, bordas.length - 1)
      const regiaoSorteada = bordas[indiceDaRegiaoSorteada]
      const sitioSorteadoNaRegiaoSorteada = obterSitioAleatorio(
        regiaoSorteada,
        maximoDesperdicioDeValoresSorteados,
        separacaoMinima,
        casasDecimais,
      )

      const valorMinimoNaRegiaoSorteada = regiaoSorteada.minimo
      const novoValorDeFuncaoSorteado = valorMinimoNaRegiaoSorteada + sitioSorteadoNaRegiaoSorteada * separacaoDecimal
      const novoValorDeFuncaoSorteadoArredondado = math.round(novoValorDeFuncaoSorteado, casasDecimais)

      valoresSorteadosDeFuncoes.push(novoValorDeFuncaoSorteadoArredondado)

      bordas = atualizaBordas(
        bordas,
        novoValorDeFuncaoSorteadoArredondado,
        indiceDaRegiaoSorteada,
        separacaoMinima,
        separacaoDecimal,
        casasDecimais,
      )
      quantidadeMaximaDeValoresPermitidos = obterQuantidadeMaximaDeValoresPermitidos(bordas)
      quantidadeValoresDemandada--
      maximoDesperdicioDeValoresSorteados = quantidadeMaximaDeValoresPermitidos - quantidadeValoresDemandada
    }
  } else {
    throw new ParticularizaError({
      id: id,
      code: codigosErros.funcaoSorteioImpossivel,
    })
  }

  return valoresSorteadosDeFuncoes
}
