# Como contribuir para o Multiprova

Nesta seção fornecemos um guia geral para que desenvolvedores contribuam com funções personalizadas que serão incluídas no [Multiprova](https://site.multiprova.ufrn.br).

Desde já agradecemos pela colaboração e colocamo-nos à disposição para ajudá-lo no que for necessário.

Antes de passar para os procedimentos necessários à codificação das novas funções, é importante conhecer a estrutura de arquivos do projeto.

## Estrutura do Projeto

A estrutura do projeto é mostrada abaixo. Para contribuir com novas funções, é necessário criar/editar arquivos que estão na pasta `biblioteca`.

```
particularizacao
│
└───biblioteca
│   │   index.js
│   │
│   └───metodosNumericos
│   │   └───raizes
│   │   │      bisseccao.js
│   │   │      bisseccao.test.js
│   │   │      suaFuncao.js
│   │   │      suaFuncao.test.js
│   │   │      ...
│   │   │      index.js
│   │   │      ...
│   │   │
│   │   └───sistemasLineares
│   │   │      ...
│   │   │      index.js
│   │   │      ...
│   │   │
│   └───suaAreaDeConheciento
│   │   └───subDivisaoArea
│   │   │      outraFuncao.js
│   │   │      outraFuncao.test.js
│   │   │      ...
│   │   │      index.js
│   │   │      ...
│   │   │
└───particularizacao
       ...
       ...
```

Considere que desejamos definir uma nova função `suaFuncao` dentro de um subdiretório existente (ex.: `raizes`). Devemos então:

- criar os arquivos `suaFuncao.js` e `suaFuncao.test.js` na pasta `raizes`;
- editar o arquivo `index.js` na pasta `raizes` para exportar a função ora criada;
- editar o arquivo `index.js` na pasta `biblioteca` para inserir a função a ser importada pelo Multiprova;
- incluir a documentação para a nova função na [wiki deste projeto](manual/Manual - Index).

## Procedimento

O passo a passo abaixo é só uma referência. Antes de iniciar a codificação, leia com atenção a seção seguinte de Boas Práticas. Considerando o exemplo acima (criação da função `suaFuncao`), deve-se fazer:

1. Clone o repositório localmente:

```Shell
git clone https://gitlab.com/tapioca-ufrn/multiprova/particularizacao.git
```

2. Crie uma nova _branch_ a partir da _branch_ `develop`:

```Shell
cd particularizacao
git checkout -b feature/suaFuncao
```

3. Crie os arquivos `suaFuncao.js` e `suaFuncao.test.js`.
4. Edite os arquivos `index.js` nas pastas `raizes` e `particularizacao`.
5. Faça um _commit_ das alterações e envie a _branch_ para o gitlab.

```Shell
git add .
git commit -m "Adicionando funções x, y e z"
git push
```

6. No gitlab.com, abra um solicitação de [_Merge request_](https://gitlab.com/tapioca-ufrn/multiprova/particularizacao/-/merge_requests) da _branch_ `feature/suaFuncao` para a _branch_ `develop`.

## Boas práticas

Segue algumas orientações para manter um estilo padronizado de e um código legível, de fácil entendimento.

1. **Siga as regras de _lint_ do projeto.** Para os usuários do [VSCode](https://code.visualstudio.com/), já existe um arquivo de configuração que força o estilo ao salvar os arquivos. Após cada edição, execute o seguinte comando:

```Shell
npm run lint
```

Somente alterações sem erros de lint podem ser adicionadas ao projeto.

2. **Valide as entradas do usuário**. O arquivo `suaFuncao.js` deve ter, no mínimo, duas funções: a função principal a ser exportada e uma função `validarEntradas()`, que valida as entradas do usuário (veja, por exemplo, [`bisseccao.js`](https://gitlab.com/tapioca-ufrn/multiprova/particularizacao/-/blob/master/biblioteca/metodosNumericos/raizes/bisseccao.js)).

3. **Previna sua função de executar loops infinitos.** Essa ação pode ser executada dentro de cada laço ou mesmo na validação das entradas do usuário, como a verificação do parâmetro `numMaxIt` na função [`bisseccao.js`](https://gitlab.com/tapioca-ufrn/multiprova/particularizacao/-/blob/master/biblioteca/metodosNumericos/raizes/bisseccao.js)).

4. **Teste integralmente o seu código.** Utilizamos o framework [Jest](https://jestjs.io/) para a realização de testes unitários. Somente funções com ~100% das linhas testadas são aceitas. Para tanto, crie um arquivo `suaFuncao.test.js` para escrever os testes. Ao escrever o teste, sempre execute o comando abaixo para testar sua função.

```Shell
npm test /caminho/para/o/arquivo/suaFuncao.test.js
```

Para saber a cobertura de testes do projeto e, em especial, da sua função, execute

```Shell
npm run test-ci
```

5. **Faça a documentação da função**. Utilizamos a wiki desse projeto para a documentação do usuário final. Dessa forma, pelo menos dois arquivos deverão ser editados: o [índice ou listagem das funções](manual/Manual - Index) e o documento que descreve a função (ex.: [bisseccao() - Método da Bissecção](manual/metodosNumericos/bisseccao)). Note que o documento que descreve a função deve possuir as seguintes seções:

   1. Descrição.
   2. Sintaxe.
   3. Parâmetros.
   4. Retorno.
   5. Exemplos.
      Para melhor padronização, faça uma cópia do exemplo e edite conforme necessário.

6. **Escrevas funções curtas**. Ao escrever `suaFuncao.js`, abuse do uso de funções de forma cada função execute uma única tarefa. Isto tornará seu código fácil de ser entendido e testado.

7. **Faça uso da documentação do mathjs**. Usamos a biblioteca [`mathjs`](https://mathjs.org/) para implementar operações de `parse` e `evaluate`, como também de validação de dados de entrada. Tenha sempre em mãos a [documentação](https://mathjs.org/docs/) ao implementar suas funções.
