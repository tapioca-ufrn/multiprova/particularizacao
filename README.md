# Multiprova - Particularização

Biblioteca usada pelo Multiprova para gerar provas particularizadas para cada estudante, através do sorteiro de variáveis e cálculo de funções.

# Instalação e uso

Para usar a biblioteca no projeto web ou no microserviço de instanciamento, instale normalmente usando npm install

```shell
npm install @multiprova/particularizacao
```

Após instalar, importe a biblioteca:

```javascript
import { particularizarQuestao } from @multiprova/particularizacao
```

Para cada questão que desejar particularizar, faça um chamado à função `particularizarQuestao` passando, como argumentos, o objeto contendo os acessórios da questão e, opcionalmente, o número máximo de tentativas (padrão 30 tentativas) de personalizar a questão:

```javascript
let resultadoParticularizacao
try {
  resultadoParticularizacao = particularizarQuestao(acessorios[, nMaximoTentativas])
} catch (erro) {
  console.log(erro.errosArray)
}
```

Em caso de erro na particularização, será retornada uma array `erro.errosArray` com a seguinte assinatura:

```JSON
[
  {
    id = 'gg6afb12-c1c0-43b9-9a96-validador2'
    code = 'conteudo_invalido_do_validador'
    info = 'x>=3'
  },
  ...
]
```

# Como contribuir

Além das funções matemáticas padrões, o Multiprova permite cadastrar funções definidas pelo usuário para serem usadas como acessórios, seja como valores calculados nas famílias de funções ou em validadores.

Para saber como contribuir, acesse a [Wiki do projeto](https://gitlab.com/tapioca-ufrn/multiprova/particularizacao/-/wikis/home).
